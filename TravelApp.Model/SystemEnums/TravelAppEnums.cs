﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravelApp.Model.SystemEnums
{
    public class TravelAppEnums
    {
        public enum Status
        {
            Active = 1,
            InActive = 2,
            Delete = 3,
        }
        public enum Roles
        {
            Admin = 1,
            Editor = 2,
            User = 3,
        }
        public enum Platform
        {
            IOS = 4,
            Android = 5,
            Web = 8,

        }
        public enum FileType
        {
            Image = 6,
            Video = 7,

        }

        public enum OperationType
        {
            Like = 1,
            View = 2,
            Share = 3,
        }

        public enum UserFollowerType
        {
            Follow = 1,
            Following = 2
        }

        public enum UserFollowerBoth
        {
            IsSinelSide = 1,
            IsBothSide = 2
        }

        public enum APIResponseEnum
        {
            Success = 1,
            Failed = 0,
            TokenExpired = 2
        }

        public enum StoryStatus
        {
            Approve = 9,
            Rejected = 10,
            Pending = 11
        }

        public enum PageName
        {
            About = 14,
            TermCondition = 15,
            ContactUs = 16
        }

        public enum NotificationType
        {
            FollowingRequest = 12,
            StoryComment = 13,
            Like = 20,
            Approved = 21

        }


    }
}
