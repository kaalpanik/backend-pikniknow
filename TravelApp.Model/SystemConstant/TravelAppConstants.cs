﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravelApp.Model.SystemConstant
{
    public static class TravelAppConstants
    {
        public const string FollowRequest = "New Following Request";
        public const string StoryComment = "Story Comment";
    }
}
