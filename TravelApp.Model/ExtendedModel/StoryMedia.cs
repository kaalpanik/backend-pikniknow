﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TravelApp.Model.DbModel
{
    public partial class StoryMedia
    {
        [NotMapped]
        public IFormFile StoryMediaPath { get; set; }

        [NotMapped]
        public int StatusId { get; set; }
    }
}
