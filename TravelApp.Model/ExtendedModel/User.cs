﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TravelApp.Model.DbModel
{
    public partial class User
    {
        [NotMapped]
        public IFormFile ProfileMultipartImage { get; set; }

        [NotMapped]
        public bool IsChangePassword { get; set; } = false;

        [NotMapped]
        public string OldPassword { get; set; } 

        [NotMapped]
        public string NewPassword { get; set; }

        [NotMapped]
        public string ConfirmPassword { get; set; } 
    }
}
