using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Rx.Core.Data.Attributes;

namespace TravelApp.Model.DbModel
{
    [Table("RolePermissions",Schema="dbo")]
    public partial class RolePermission
    {
		#region RolePermissionId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
		#endregion RolePermissionId Annotations

        public int RolePermissionId { get; set; }

		#region RoleId Annotations

        [Range(1,int.MaxValue)]
        [Required]
        [RelationshipTableAttribue("Roles","dbo","","RoleId")]
		#endregion RoleId Annotations

        public int RoleId { get; set; }

		#region ApplicationModuleId Annotations

        [Range(1,int.MaxValue)]
        [Required]
        [RelationshipTableAttribue("ApplicationModules","dbo","","ApplicationModuleId")]
		#endregion ApplicationModuleId Annotations

        public int ApplicationModuleId { get; set; }

		#region CanView Annotations

        [Required]
		#endregion CanView Annotations

        public bool CanView { get; set; }

		#region CanAdd Annotations

        [Required]
		#endregion CanAdd Annotations

        public bool CanAdd { get; set; }

		#region CanEdit Annotations

        [Required]
		#endregion CanEdit Annotations

        public bool CanEdit { get; set; }

		#region CanDelete Annotations

        [Required]
		#endregion CanDelete Annotations

        public bool CanDelete { get; set; }

		#region ApplicationModule Annotations

        [ForeignKey(nameof(ApplicationModuleId))]
		#endregion ApplicationModule Annotations

        public virtual ApplicationModule ApplicationModule { get; set; }

		#region Role Annotations

        [ForeignKey(nameof(RoleId))]
		#endregion Role Annotations

        public virtual Role Role { get; set; }


        public RolePermission()
        {
        }
	}
}