using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelApp.Model.DbModel
{
    [Table("Tags",Schema="dbo")]
    public partial class Tag
    {
		#region TagId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
		#endregion TagId Annotations

        public int TagId { get; set; }

		#region Name Annotations

        [Required]
        [MaxLength(50)]
		#endregion Name Annotations

        public string Name { get; set; }

		#region StatusId Annotations

        [Range(1,int.MaxValue)]
        [Required]
		#endregion StatusId Annotations

        public int StatusId { get; set; }


        public Tag()
        {
        }
	}
}