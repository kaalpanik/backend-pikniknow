using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Rx.Core.Data.Attributes;
namespace TravelApp.Model.DbModel
{
    [Table("Stories",Schema="dbo")]
    public partial class Story
    {
		#region StoryId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
		#endregion StoryId Annotations

        public int StoryId { get; set; }

		#region Title Annotations

        [Required]
        [MaxLength(50)]
		#endregion Title Annotations

        public string Title { get; set; }

		#region Latitude Annotations

        [Required]
        [MaxLength(30)]
		#endregion Latitude Annotations

        public string Latitude { get; set; }

		#region Longitude Annotations

        [Required]
        [MaxLength(30)]
		#endregion Longitude Annotations

        public string Longitude { get; set; }

		#region SpotInformation Annotations

        [Required]
        [MaxLength(500)]
		#endregion SpotInformation Annotations

        public string SpotInformation { get; set; }

		#region CreatedByDate Annotations

        [Required]
		#endregion CreatedByDate Annotations

        public System.DateTime CreatedByDate { get; set; }

		#region CreatedById Annotations

        [Range(1,int.MaxValue)]
        [Required]
		#endregion CreatedById Annotations

        public int CreatedById { get; set; }


        public Nullable<int> ModifiedById { get; set; }


        public Nullable<System.DateTime> ModifiedByDate { get; set; }

        #region StatusId Annotations

        [Range(1, int.MaxValue)]
        [Required]
        #endregion StatusId Annotations

        public int StatusId { get; set; }

		#region TotalLike Annotations

       // [Range(1,int.MaxValue)]
        [Required]
		#endregion TotalLike Annotations

        public int TotalLike { get; set; } = 0;

        #region TotalShare Annotations

        //[Range(1,int.MaxValue)]
        [Required]
		#endregion TotalShare Annotations

        public int TotalShare { get; set; } = 0;

        #region TotalShare Annotations

        //[Range(1,int.MaxValue)]
        [Required]
        #endregion TotalShare Annotations

        public int TotalView { get; set; } = 0;


        #region StoryMedias Annotations

        [InverseProperty("Story")]
		#endregion StoryMedias Annotations

        public virtual ICollection<StoryMedia> StoryMedias { get; set; }

		#region StoryTags Annotations

        [InverseProperty("Story")]
		#endregion StoryTags Annotations

        public virtual ICollection<StoryTag> StoryTags { get; set; }

		#region StoryComments Annotations

        [InverseProperty("Story")]
		#endregion StoryComments Annotations

        public virtual ICollection<StoryComment> StoryComments { get; set; }

        [MaxLength(50)]
        public string City { get; set; }

        [MaxLength(50)]
        public string State { get; set; }

        [MaxLength(50)]
        public string Country { get; set; }

        [MaxLength(50)]
        public string PostalCode { get; set; }

        [MaxLength(100)]
        public string AddressLine1 { get; set; }

        [MaxLength(100)]
        public string AddressLine2 { get; set; }

        [MaxLength(100)]
        public string LocationName { get; set; }

        public string MetaDescription { get; set; }

        public Story()
        {
			StoryMedias = new HashSet<StoryMedia>();
			StoryTags = new HashSet<StoryTag>();
			StoryComments = new HashSet<StoryComment>();
        }
	}
}