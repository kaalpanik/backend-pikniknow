using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Rx.Core.Data.Attributes;
namespace TravelApp.Model.DbModel
{
    [Table("Countries",Schema="dbo")]
    public partial class Country
    {
		#region CountryId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
		#endregion CountryId Annotations

        public int CountryId { get; set; }

		#region DefaultLanguageId Annotations

        [Range(1,int.MaxValue)]
        [Required]
		#endregion DefaultLanguageId Annotations

        public int DefaultLanguageId { get; set; }

		#region CountryName Annotations

        [Required]
        [MaxLength(50)]
		#endregion CountryName Annotations

        public string CountryName { get; set; }

		#region CountryCode Annotations

        [Required]
        [MaxLength(50)]
		#endregion CountryCode Annotations

        public string CountryCode { get; set; }

		#region DateFormat Annotations

        [MaxLength(50)]
		#endregion DateFormat Annotations

        public string DateFormat { get; set; }

		#region DateFormatSeperator Annotations

        [MaxLength(50)]
		#endregion DateFormatSeperator Annotations

        public string DateFormatSeperator { get; set; }

		#region CurrencyFormat Annotations

        [MaxLength(20)]
		#endregion CurrencyFormat Annotations

        public string CurrencyFormat { get; set; }

		#region DecimalSeperator Annotations

        [MaxLength(50)]
		#endregion DecimalSeperator Annotations

        public string DecimalSeperator { get; set; }

		#region PhoneFormat Annotations

        [MaxLength(50)]
		#endregion PhoneFormat Annotations

        public string PhoneFormat { get; set; }

		#region PostalCodeFormat Annotations

        [MaxLength(50)]
		#endregion PostalCodeFormat Annotations

        public string PostalCodeFormat { get; set; }

		#region Active Annotations

        [Required]
		#endregion Active Annotations

        public bool Active { get; set; }


        public Country()
        {
        }
	}
}