using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Rx.Core.Data.Attributes;
namespace TravelApp.Model.DbModel
{
    [Table("ApplicationUserTokens",Schema="dbo")]
    public partial class ApplicationUserToken
    {
		#region ApplicationUserTokenId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
		#endregion ApplicationUserTokenId Annotations

        public int ApplicationUserTokenId { get; set; }

		#region UserId Annotations

        [Range(1,int.MaxValue)]
        [Required]
		#endregion UserId Annotations

        public int UserId { get; set; }

		#region SecurityKey Annotations

        [Required]
        [MaxLength(32)]
		#endregion SecurityKey Annotations

        public byte[] SecurityKey { get; set; }

		#region JwtToken Annotations

        [Required]
		#endregion JwtToken Annotations

        public string JwtToken { get; set; }

		#region TokenIssuer Annotations

        [Required]
        [MaxLength(50)]
		#endregion TokenIssuer Annotations

        public string TokenIssuer { get; set; }

		#region AccessedPlatform Annotations

        [Required]
        [MaxLength(100)]
		#endregion AccessedPlatform Annotations

        public string AccessedPlatform { get; set; }

		#region CreatedDateTime Annotations

        [Required]
		#endregion CreatedDateTime Annotations

        public System.DateTime CreatedDateTime { get; set; }

		#region ExpiresAt Annotations

        [Required]
		#endregion ExpiresAt Annotations

        public System.DateTime ExpiresAt { get; set; }

		#region IsActive Annotations

        [Required]
		#endregion IsActive Annotations

        public bool IsActive { get; set; }

		#region IPAddress Annotations

        [MaxLength(50)]
		#endregion IPAddress Annotations

        public string IPAddress { get; set; }


        public Nullable<int> PlatformId { get; set; }

		#region BrowserName Annotations

        [MaxLength(50)]
		#endregion BrowserName Annotations

        public string BrowserName { get; set; }

		#region AppVersion Annotations

        [MaxLength(50)]
		#endregion AppVersion Annotations

        public string AppVersion { get; set; }

		#region DeviceName Annotations

        [MaxLength(50)]
		#endregion DeviceName Annotations

        public string DeviceName { get; set; }

		#region DeviceOSVersion Annotations

        [MaxLength(100)]
		#endregion DeviceOSVersion Annotations

        public string DeviceOSVersion { get; set; }

		#region DeviceID Annotations

        [MaxLength(50)]
		#endregion DeviceID Annotations

        public string DeviceID { get; set; }


        public string FCMToken { get; set; }


        public ApplicationUserToken()
        {
        }
	}
}