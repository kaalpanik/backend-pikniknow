﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Model.DbModel
{
    [Table("UserStories", Schema = "dbo")]
    public class UserStory
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]


        public int UserStoryId  { get; set; }

        [Range(1, int.MaxValue)]
        [Required]
        public int UserId { get; set; }

        [Range(1, int.MaxValue)]
        [Required]
        public int StoryId { get; set; }

        [Required]
        public OperationType TypeId { get; set; }
    }
}
