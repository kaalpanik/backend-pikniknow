using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Rx.Core.Data.Attributes;
namespace TravelApp.Model.DbModel
{
    [Table("GlobalSettings",Schema="dbo")]
    public partial class GlobalSetting
    {
		#region ConfigurationId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
		#endregion ConfigurationId Annotations

        public int ConfigurationId { get; set; }

		#region RecordLock Annotations

        [Required]
		#endregion RecordLock Annotations

        public bool RecordLock { get; set; }

		#region LockDuration Annotations

        [MaxLength(10)]
		#endregion LockDuration Annotations

        public string LockDuration { get; set; }

		#region ApplicationTimeZoneId Annotations

        [Range(1,int.MaxValue)]
        [Required]
        [RelationshipTableAttribue("ApplicationTimeZones","dbo","","ApplicationTimeZoneId")]
		#endregion ApplicationTimeZoneId Annotations

        public int ApplicationTimeZoneId { get; set; }

		#region LanguageId Annotations

        [Range(1,int.MaxValue)]
        [Required]
		#endregion LanguageId Annotations

        public int LanguageId { get; set; }

		#region RequestLogging Annotations

        [Required]
		#endregion RequestLogging Annotations

        public bool RequestLogging { get; set; }

		#region SocialAuth Annotations

        [Required]
		#endregion SocialAuth Annotations

        public bool SocialAuth { get; set; }

		#region TwoFactorAuthentication Annotations

        [Required]
		#endregion TwoFactorAuthentication Annotations

        public bool TwoFactorAuthentication { get; set; }

		#region AutoTranslation Annotations

        [Required]
		#endregion AutoTranslation Annotations

        public bool AutoTranslation { get; set; }

		#region ApplicationTimeZone Annotations

        [ForeignKey(nameof(ApplicationTimeZoneId))]
		#endregion ApplicationTimeZone Annotations

        public virtual ApplicationTimeZone ApplicationTimeZone { get; set; }


        public GlobalSetting()
        {
        }
	}
}