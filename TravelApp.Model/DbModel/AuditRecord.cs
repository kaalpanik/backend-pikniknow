using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Rx.Core.Data.Attributes;
namespace TravelApp.Model.DbModel
{
    [Table("AuditRecords",Schema="dbo")]
    public partial class AuditRecord
    {
		#region AuditRecordId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
		#endregion AuditRecordId Annotations

        public int AuditRecordId { get; set; }


        public Nullable<int> AuditRequestId { get; set; }

		#region EventType Annotations

        [Required]
        [MaxLength(9)]
		#endregion EventType Annotations

        public string EventType { get; set; }

		#region TableName Annotations

        [Required]
        [MaxLength(50)]
		#endregion TableName Annotations

        public string TableName { get; set; }

		#region RecordId Annotations

        [Required]
        [MaxLength(100)]
		#endregion RecordId Annotations

        public string RecordId { get; set; }

		#region RecordName Annotations

        [Required]
		#endregion RecordName Annotations

        public string RecordName { get; set; }


        public string OldValue { get; set; }

		#region NewValue Annotations

        [Required]
		#endregion NewValue Annotations

        public string NewValue { get; set; }


        public AuditRecord()
        {
        }
	}
}