using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Rx.Core.Data.Attributes;
namespace TravelApp.Model.DbModel
{
    [Table("Roles",Schema="dbo")]
    public partial class Role
    {
		#region RoleId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
		#endregion RoleId Annotations

        public int RoleId { get; set; }

		#region RoleName Annotations

        [Required]
        [MaxLength(50)]
		#endregion RoleName Annotations

        public string RoleName { get; set; }

		#region Status Annotations

        [Range(1,int.MaxValue)]
        [Required]
		#endregion Status Annotations

       /// public Status Status { get; set; }

		#region RolePermissions Annotations

        [InverseProperty("Role")]
		#endregion RolePermissions Annotations

        public virtual ICollection<RolePermission> RolePermissions { get; set; }


        public Role()
        {
			RolePermissions = new HashSet<RolePermission>();
        }
	}
}