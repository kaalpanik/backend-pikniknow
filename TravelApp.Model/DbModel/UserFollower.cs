using Rx.Core.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace TravelApp.Model.DbModel
{
    [Table("UserFollowers",Schema="dbo")]
    public partial class UserFollower
    {
		#region UserFollowerId Annotations

        [System.ComponentModel.DataAnnotations.Key]
		#endregion UserFollowerId Annotations

        public int UserFollowerId { get; set; }

		#region UserFollowerFriendId Annotations

        [Range(1,int.MaxValue)]
        [Required]
        [RelationshipTableAttribue("Users","dbo","","UserFollowerFriendId")]
		#endregion UserFollowerFriendId Annotations

        public int UserFollowerFriendId { get; set; }

		#region IsFollower Annotations

        [Required]
		#endregion IsFollower Annotations

        public int TypeId { get; set; }

        public int StatusId { get; set; }

        #region CreatedById Annotations

        [Range(1,int.MaxValue)]
        [Required]
        [RelationshipTableAttribue("Users","dbo","","CreatedById")]
		#endregion CreatedById Annotations

        public int CreatedById { get; set; }

		#region CreatedByDate Annotations

        [Required]
		#endregion CreatedByDate Annotations

        public System.DateTime CreatedByDate { get; set; }


        public Nullable<int> ModifiedById { get; set; }


        public Nullable<System.DateTime> ModifiedByDate { get; set; }

		#region UserFollowerFriend Annotations

        [ForeignKey(nameof(UserFollowerFriendId))]
		#endregion UserFollowerFriend Annotations

        public virtual User UserFollowerFriend { get; set; }

		#region CreatedBy Annotations

        [ForeignKey(nameof(CreatedById))]
		#endregion CreatedBy Annotations

        public virtual User CreatedBy { get; set; }


        public UserFollower()
        {
        }
	}
}