using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace TravelApp.Model.DbModel
{
    [Table("SMTPConfigurations",Schema="dbo")]
    public partial class SMTPConfiguration
    {
		#region SmtpConfigurationId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
		#endregion SmtpConfigurationId Annotations

        public int SmtpConfigurationId { get; set; }

		#region FromEmail Annotations

        [Required]
        [MaxLength(200)]
		#endregion FromEmail Annotations

        public string FromEmail { get; set; }


        public Nullable<bool> DefaultCredentials { get; set; }


        public Nullable<bool> EnableSSL { get; set; }

		#region Host Annotations

        [Required]
        [MaxLength(100)]
		#endregion Host Annotations

        public string Host { get; set; }

		#region UserName Annotations

        [MaxLength(200)]
		#endregion UserName Annotations

        public string UserName { get; set; }

		#region Password Annotations

        [MaxLength(100)]
		#endregion Password Annotations

        public string Password { get; set; }

		#region Port Annotations

        [Range(1,int.MaxValue)]
        [Required]
		#endregion Port Annotations

        public int Port { get; set; }

		#region DeliveryMethod Annotations

        [MaxLength(100)]
		#endregion DeliveryMethod Annotations

        public string DeliveryMethod { get; set; }

		#region SendIndividually Annotations

        [Required]
		#endregion SendIndividually Annotations

        public bool SendIndividually { get; set; }


        public Nullable<bool> IsActive { get; set; }


        public SMTPConfiguration()
        {
        }
	}
}