﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelApp.Model.DbModel
{
    [Table("Banners", Schema = "dbo")]
    public class Banner
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
        public int BannerId { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        public string MetaDescription { get; set; }

        [Range(1, int.MaxValue)]
        [Required]
        public int StatusId { get; set; }

        [Range(1, int.MaxValue)]
        [Required]
        public int OrderId { get; set; }

        [Range(1, int.MaxValue)]
        [Required]
        public int CreatedById { get; set; }

        [Required]
        public DateTime CreatedByDate { get; set; }


        public DateTime? ModifiedByDate { get; set; }

        public int? ModifiedById { get; set; }

        public string ImageURL { get; set; }
        public string RedirectURL { get; set; }
        [NotMapped]
        public IFormFile BannerImage { get; set; }
    }
}
