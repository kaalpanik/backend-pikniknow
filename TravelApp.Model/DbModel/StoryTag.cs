using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Rx.Core.Data.Attributes;

namespace TravelApp.Model.DbModel
{
    [Table("StoryTags",Schema="dbo")]
    public partial class StoryTag
    {
		#region StoryTagId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
		#endregion StoryTagId Annotations

        public int StoryTagId { get; set; }

		#region TagId Annotations

       // [Range(1,int.MaxValue)]
        [Required]
		#endregion TagId Annotations

        public int TagId { get; set; }

		#region StoryId Annotations

        [RelationshipTableAttribue("Stories","dbo","","StoryId")]
		#endregion StoryId Annotations

        public int StoryId { get; set; }

		#region Story Annotations

        [ForeignKey(nameof(StoryId))]
		#endregion Story Annotations

        public virtual Story Story { get; set; }

        [NotMapped]
        public int StatusId { get; set; }

        [NotMapped]
        public string Name { get; set; }

        public StoryTag()
        {
        }
	}
}