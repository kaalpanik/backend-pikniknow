﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TravelApp.Model.DbModel
{
    [Table("vApplicationUserTokens", Schema = "dbo")]
    public class vApplicationUserToken
    {
        [System.ComponentModel.DataAnnotations.Key]
        public int ApplicationUserTokenId { get; set; }
        public int UserId { get; set; }
        public string FCMToken { get; set; }
    }
}
