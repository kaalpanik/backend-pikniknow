using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Rx.Core.Data.Attributes;
namespace TravelApp.Model.DbModel
{
    [Table("ApplicationTimeZones",Schema="dbo")]
    public partial class ApplicationTimeZone
    {
		#region ApplicationTimeZoneId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
		#endregion ApplicationTimeZoneId Annotations

        public int ApplicationTimeZoneId { get; set; }

		#region CountryId Annotations

        [Range(1,int.MaxValue)]
        [Required]
		#endregion CountryId Annotations

        public int CountryId { get; set; }

		#region ApplicationTimeZoneName Annotations

        [Required]
        [MaxLength(100)]
		#endregion ApplicationTimeZoneName Annotations

        public string ApplicationTimeZoneName { get; set; }

		#region Comment Annotations

        [Required]
        [MaxLength(200)]
		#endregion Comment Annotations

        public string Comment { get; set; }

		#region Active Annotations

        [Required]
		#endregion Active Annotations

        public bool Active { get; set; }

		#region Offset Annotations

        [MaxLength(10)]
		#endregion Offset Annotations

        public string Offset { get; set; }

		#region GlobalSettings Annotations

        [InverseProperty("ApplicationTimeZone")]
		#endregion GlobalSettings Annotations

        public virtual ICollection<GlobalSetting> GlobalSettings { get; set; }


        public ApplicationTimeZone()
        {
			GlobalSettings = new HashSet<GlobalSetting>();
        }
	}
}