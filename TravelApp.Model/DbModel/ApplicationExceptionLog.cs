using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Rx.Core.Data.Attributes;

namespace TravelApp.Model.DbModel
{
    [Table("ApplicationExceptionLogs",Schema="dbo")]
    public partial class ApplicationExceptionLog
    {
		#region ApplicationExceptionLogId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
		#endregion ApplicationExceptionLogId Annotations

        public int ApplicationExceptionLogId { get; set; }

		#region UserId Annotations

        [Range(1,int.MaxValue)]
        [Required]
		#endregion UserId Annotations

        public int UserId { get; set; }


        public Nullable<int> ApplicationTimeZoneId { get; set; }


        public Nullable<int> ApplicationModuleId { get; set; }

		#region Url Annotations

        [Required]
        [MaxLength(200)]
		#endregion Url Annotations

        public string Url { get; set; }

		#region RequestMethod Annotations

        [MaxLength(10)]
		#endregion RequestMethod Annotations

        public string RequestMethod { get; set; }

		#region Message Annotations

        [Required]
		#endregion Message Annotations

        public string Message { get; set; }

		#region ExceptionType Annotations

        [Required]
		#endregion ExceptionType Annotations

        public string ExceptionType { get; set; }

		#region ExceptionSource Annotations

        [Required]
		#endregion ExceptionSource Annotations

        public string ExceptionSource { get; set; }

		#region StackTrace Annotations

        [Required]
		#endregion StackTrace Annotations

        public string StackTrace { get; set; }

		#region InnerException Annotations

        [Required]
		#endregion InnerException Annotations

        public string InnerException { get; set; }

		#region ExceptionDate Annotations

        [Required]
		#endregion ExceptionDate Annotations

        public System.DateTime ExceptionDate { get; set; }


        public string IOTMessage { get; set; }


        public ApplicationExceptionLog()
        {
        }
	}
}