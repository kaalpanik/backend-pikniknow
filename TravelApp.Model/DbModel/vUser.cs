﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TravelApp.Model.DbModel
{
    [Table("vUsers", Schema = "dbo")]
    public class vUser
    {
        [System.ComponentModel.DataAnnotations.Key]
        public int UserId { get; set; }
        public bool IsNotificationEnable { get; set; }
        public string EmailId { get; set; }
        public string UserName { get; set; }
        public string ProfilePicturePath { get; set; }
        public int RoleId { get; set; }
    }
}
