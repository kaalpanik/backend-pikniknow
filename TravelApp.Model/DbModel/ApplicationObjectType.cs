using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Rx.Core.Data.Attributes;
namespace TravelApp.Model.DbModel
{
    [Table("ApplicationObjectTypes",Schema="dbo")]
    public partial class ApplicationObjectType
    {
		#region ApplicationObjectTypeId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
		#endregion ApplicationObjectTypeId Annotations

        public int ApplicationObjectTypeId { get; set; }

		#region ApplicationObjectTypeName Annotations

        [Required]
        [MaxLength(100)]
		#endregion ApplicationObjectTypeName Annotations

        public string ApplicationObjectTypeName { get; set; }


        public ApplicationObjectType()
        {
        }
	}
}