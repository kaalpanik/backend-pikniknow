﻿using Rx.Core.Data.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TravelApp.Model.DbModel
{
    [Table("PushNotifications", Schema = "dbo")]
    [Serializable]
    public partial class PushNotification
    {
        [Required]
        [Column("CreatedOn", TypeName = "smalldatetime")]
        public System.DateTime CreatedOn { get; set; }


        [Range(1, int.MaxValue)]
        [Column("CreatedBy")]
        public int CreatedBy { get; set; }


        [Required]
        [Column("NotificationContent")]
        public string NotificationContent { get; set; }


        [Column("Heading")]
        [MaxLength(500)]
        public string Heading { get; set; }


        [Column("ModifiedOn", TypeName = "smalldatetime")]
        public Nullable<System.DateTime> ModifiedOn { get; set; }


        [Column("ModifiedBy")]
        public Nullable<int> ModifiedBy { get; set; }


        [Required]
        [Column("Name")]
        [MaxLength(50)]
        public string Name { get; set; }


        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("PushNotificationId")]
        [TableKeyAttribute]
        public int PushNotificationId { get; set; }


        [Range(1, int.MaxValue)]
        [Column("PageLocationId")]
        public int PageLocationId { get; set; }


        [Range(1, int.MaxValue)]
        [Column("StatusId")]
        public int StatusId { get; set; }
        public PushNotification()
        {
        }

    }
}
