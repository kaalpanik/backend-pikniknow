using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Rx.Core.Security;

namespace TravelApp.Model.DbModel
{
    [Table("Users", Schema = "dbo")]
    public partial class User
    {
        #region UserId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
        #endregion UserId Annotations

        public int UserId { get; set; }

        #region UserName Annotations

        //  [Required]
        [MaxLength(50)]
        #endregion UserName Annotations

        public string UserName { get; set; }

        #region RoleId Annotations

        [Range(1, int.MaxValue)]
        [Required]
        #endregion RoleId Annotations

        public int RoleId { get; set; }

        #region MobileNumber Annotations

        [MaxLength(12)]
        #endregion MobileNumber Annotations

        public string MobileNumber { get; set; }

        #region ProfilePicturePath Annotations

        [MaxLength(200)]
        #endregion ProfilePicturePath Annotations

        public string ProfilePicturePath { get; set; }

        #region Bio Annotations

        [MaxLength(300)]
        #endregion Bio Annotations

        public string Bio { get; set; }

        #region Latitude Annotations

        [MaxLength(30)]
        #endregion Latitude Annotations

        public string Latitude { get; set; }

        #region Longitude Annotations

        [MaxLength(30)]
        #endregion Longitude Annotations

        public string Longitude { get; set; }

        #region City Annotations

        //    [Required]
        [MaxLength(20)]
        #endregion City Annotations

        public string City { get; set; }

        #region State Annotations

        // [Required]
        [MaxLength(20)]
        #endregion State Annotations

        public string State { get; set; }

        public bool IsNotificationEnable { get; set; }
        #region EmailId Annotations

        [MaxLength(50)]
        #endregion EmailId Annotations

        public string EmailId { get; set; }

        #region CreatedByDate Annotations

        [Required]
        #endregion CreatedByDate Annotations

        public System.DateTime CreatedByDate { get; set; } = DateTime.Now;


        public Nullable<int> ModifiedById { get; set; }


        public Nullable<System.DateTime> ModifiedByDate { get; set; }

        #region CreatedById Annotations

        [Range(1, int.MaxValue)]
        [Required]
        #endregion CreatedById Annotations 

        public int CreatedById { get; set; } = 1;

        #region StatusId Annotations

        [Range(1, int.MaxValue)]
        [Required]
        #endregion StatusId Annotations

        public int StatusId { get; set; }

        #region OtpCode Annotations


        [MaxLength(50)]
        #endregion OtpCode Annotations

        public string OtpCode { get; set; }

        #region UserFollowers Annotations

        [InverseProperty("UserFollowerFriend")]
        #endregion UserFollowers Annotations

        public virtual ICollection<UserFollower> UserFollowers { get; set; }

        #region UserFollowersCreatedBy Annotations

        [InverseProperty("CreatedBy")]
        #endregion UserFollowersCreatedBy Annotations

        public virtual ICollection<UserFollower> UserFollowersCreatedBy { get; set; }

        public byte[] Password { get; set; }

        public byte[] Salt { get; set; }
        [MaxLength(100)]
        public string SessionId { get; set; }
        public string UId { get; set; }
        public bool IsProfileSubmitted { get; set; } = false;

        [Column("VerificationCode")]
        public Nullable<Guid> VerificationCode { get; set; }
        public User()
        {
            UserFollowers = new HashSet<UserFollower>();
            UserFollowersCreatedBy = new HashSet<UserFollower>();
        }
    }
}