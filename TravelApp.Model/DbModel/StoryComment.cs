using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Rx.Core.Data.Attributes;
namespace TravelApp.Model.DbModel
{
    [Table("StoryComments",Schema="dbo")]
    public partial class StoryComment
    {
		#region StoryCommentId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
		#endregion StoryCommentId Annotations

        public int StoryCommentId { get; set; }

		#region StoryId Annotations

        [Range(1,int.MaxValue)]
        [Required]
        [RelationshipTableAttribue("Stories","dbo","","StoryId")]
		#endregion StoryId Annotations

        public int StoryId { get; set; }

		#region Comment Annotations

        [Required]
        [MaxLength(500)]
		#endregion Comment Annotations

        public string Comment { get; set; }

		#region IsReplay Annotations

        [Required]
		#endregion IsReplay Annotations

        public bool IsReplay { get; set; }

		#region CreatedById Annotations

        [Range(1,int.MaxValue)]
        [Required]
		#endregion CreatedById Annotations

        public int CreatedById { get; set; }

		#region CreatedByDate Annotations

        [Required]
		#endregion CreatedByDate Annotations

        public System.DateTime CreatedByDate { get; set; }


        public Nullable<int> ModifiedById { get; set; }


        public Nullable<System.DateTime> ModifiedByDate { get; set; }

		#region Story Annotations

        [ForeignKey(nameof(StoryId))]
		#endregion Story Annotations

        public virtual Story Story { get; set; }

        public int ParentStoryCommentId { get; set; } = 0;
        public StoryComment()
        {
        }
	}
}