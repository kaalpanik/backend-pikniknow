﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelApp.Model.DbModel
{
    [Table("vStories", Schema = "dbo")]
    public class vStories
    {
        [System.ComponentModel.DataAnnotations.Key]
        public int StoryId { get; set; }
        public string Title { get; set; }
        public string UserName { get; set; }
        public int CreatedById { get; set; }
        public DateTime? CreatedByDate { get; set; }

    }
}
