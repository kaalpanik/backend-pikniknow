using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Rx.Core.Data.Attributes;
namespace TravelApp.Model.DbModel
{
    [Table("RequestLogs",Schema="dbo")]
    public partial class RequestLog
    {
		#region RequestLogId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
		#endregion RequestLogId Annotations

        public int RequestLogId { get; set; }


        public Nullable<int> UserId { get; set; }


        public Nullable<int> ApplicationModuleId { get; set; }

		#region RecordId Annotations

        [MaxLength(100)]
		#endregion RecordId Annotations

        public string RecordId { get; set; }

		#region RequestMethod Annotations

        [Required]
        [MaxLength(10)]
		#endregion RequestMethod Annotations

        public string RequestMethod { get; set; }

		#region ServiceUri Annotations

        [Required]
        [MaxLength(100)]
		#endregion ServiceUri Annotations

        public string ServiceUri { get; set; }

		#region ClientIPAddress Annotations

        [MaxLength(50)]
		#endregion ClientIPAddress Annotations

        public string ClientIPAddress { get; set; }

		#region BrowserName Annotations

        [MaxLength(200)]
		#endregion BrowserName Annotations

        public string BrowserName { get; set; }

		#region RequestTime Annotations

        [Required]
		#endregion RequestTime Annotations

        public System.DateTime RequestTime { get; set; }

		#region TotalDuration Annotations

        [Required]
		#endregion TotalDuration Annotations

        public TimeSpan TotalDuration { get; set; }

		#region Parameters Annotations

        [Required]
		#endregion Parameters Annotations

        public string Parameters { get; set; }


        public Nullable<int> ContentLength { get; set; }


        public string Cookies { get; set; }


        public string AuthorizationHeader { get; set; }

		#region ResponseStatusCode Annotations

        [Range(1,int.MaxValue)]
        [Required]
		#endregion ResponseStatusCode Annotations

        public int ResponseStatusCode { get; set; }


        public RequestLog()
        {
        }
	}
}