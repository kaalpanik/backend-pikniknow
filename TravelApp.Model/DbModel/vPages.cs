﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelApp.Model.DbModel
{
    [Table("vPages", Schema = "dbo")]
    public class vPages
    {
        [System.ComponentModel.DataAnnotations.Key]
        public int PageId { get; set; }
        public string Heading { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public string PageName { get; set; }
        public int PageLocationId { get; set; }

    }
}
