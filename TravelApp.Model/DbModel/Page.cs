﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelApp.Model.DbModel
{
    [Table("Pages", Schema = "dbo")]
    public class Page
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
        public int PageId { get; set; }

        [Required]
        public string Heading { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [Range(1, int.MaxValue)]
        [Required]
        public int StatusId { get; set; }

        [Range(1, int.MaxValue)]
        [Required]
        public int PageLocationId { get; set; }

        [Range(1, int.MaxValue)]
        [Required]
        public int CreatedById { get; set; }

        [Required]
        public DateTime CreatedByDate { get; set; }


        public DateTime? ModifiedByDate { get; set; }

        public int? ModifiedById { get; set; }
    }
}
