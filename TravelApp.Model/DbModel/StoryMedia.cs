using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Rx.Core.Data.Attributes;
namespace TravelApp.Model.DbModel
{
    [Table("StoryMedias",Schema="dbo")]
    public partial class StoryMedia
    {
		#region StoryMediaId Annotations

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
		#endregion StoryMediaId Annotations

        public int StoryMediaId { get; set; }

		#region StoryId Annotations

    //    [Range(1,int.MaxValue)]
        [Required]
        [RelationshipTableAttribue("Stories","dbo","","StoryId")]
		#endregion StoryId Annotations

        public int StoryId { get; set; }

		#region FileTypeId Annotations

        [Range(1,int.MaxValue)]
        [Required]
		#endregion FileTypeId Annotations

        public int FileTypeId { get; set; }

		#region FilePath Annotations

   //     [Required]
        [MaxLength(500)]
		#endregion FilePath Annotations

        public string FilePath { get; set; }

		#region IsPrimaryMedia Annotations

        [Required]
		#endregion IsPrimaryMedia Annotations

        public bool IsPrimaryMedia { get; set; }

		#region Story Annotations

        [ForeignKey(nameof(StoryId))]
		#endregion Story Annotations

        public virtual Story Story { get; set; }


        public StoryMedia()
        {
        }
	}
}