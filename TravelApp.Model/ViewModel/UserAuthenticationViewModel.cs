﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravelApp.Model.ViewModel
{
    public class UserAuthenticationViewModel
    {
        public string Token { get; set; }

        public string FullName { get; set; }
        public string EmailId { get; set; }

        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public int FailedCount { get; set; }

        public bool IsTemporaryPassword { get; set; }

        public bool IsFirstTime { get; set; }
        public int MaxFailedCount { get; set; }
        public bool FailedLogin { get; set; }

        public string ValidationMessage { get; set; }

        public DateTime DateData { get; set; }

        public int UserId { get; set; }
        public int Status { get; set; } = 1;
        public string Otp { get; set; }

        public bool IsProfileUpdated { get; set; }
    }
}
