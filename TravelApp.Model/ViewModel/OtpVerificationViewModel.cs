﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravelApp.Model.ViewModel
{
    public class OtpVerificationViewModel
    {
        public string Otp { get; set; }
        public bool IsRegister { get; set; }
        public int UserId { get; set; }
    }
}
