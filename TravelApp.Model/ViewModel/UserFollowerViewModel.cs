﻿using System;
using System.Collections.Generic;
using System.Text;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Model.ViewModel
{
    public class UserFollowerViewModel
    {
        public int UserFollowerFriendId { get; set; }
        public int CreatedById { get; set; }
        public UserFollowerType UserFollowerTypeId { get; set; }
        public bool UnFollow { get; set; }
        public bool IsFollowBack { get; set; }

    }
}
