﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravelApp.Model.ViewModel
{
    public class PasswordResult
    {
        public byte[] Signature { get; set; }

        public byte[] Salt { get; set; }
    }
}
