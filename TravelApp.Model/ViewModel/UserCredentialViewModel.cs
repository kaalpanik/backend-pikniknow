﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravelApp.Model.ViewModel
{
    public class UserCredentialViewModel
    {
        public string MobileNumber { get; set; }
        public string Otp { get; set; }
        public DateTime DateData { get; set; }
        public string ApiKey { get; set; }
        public int FailedCount { get; set; }
        public int UserId { get; set; }
        public string IPAddress { get; set; }
        public int PlatformId { get; set; }
        public string BrowserName { get; set; }
        public string AppVersion { get; set; }
        public string DeviceName { get; set; }
        public string DeviceOSVersion { get; set; }
        public string DeviceID { get; set; }
        public string ClientURL { get; set; }

        public string FCMToken { get; set; }
        public bool IsAdmin { get; set; } = false;
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Email { get; set; }

        public string UserName { get; set; }
        public string ProfilePicturePath { get; set; }
        public string UId { get; set; }
        public Nullable<Guid> VerificationCode { get; set; }
    }
}
