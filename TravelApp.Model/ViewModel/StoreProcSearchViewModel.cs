using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Model.ViewModel
{
  public class StoreProcSearchViewModel
  {
    [Key]
    public int Id { get; set; }
    public string Result { get; set; }
  }
}
