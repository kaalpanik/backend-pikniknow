﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravelApp.Model.ViewModel
{
    public class FileDetailModel
    {
        public string FileName { get; set; }
        public string FileExtention { get; set; }
        public string FileData { get; set; }
        public string FileType { get; set; }
    }
}
