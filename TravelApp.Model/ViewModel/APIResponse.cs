﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Model.ViewModel
{
    public class APIResponse
    {
        public object Response { get; set; }
        public HashSet<string> Messages { get; set; }
        public string Message { get; set; }
        public bool Status { get; set; }

        public APIResponseEnum StatusCode { get; set; }

        public APIResponse()
        {
            Status = false;
        }
    }
}
