﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Model.ViewModel
{
    public class StoryViewModel
    {
        [Range(1, int.MaxValue)]
        public int StoryId { get; set; }
        public bool IsView { get; set; }
        public int UserId { get; set; }
        public bool IsLike { get; set; }
        public OperationType OperationType { get; set; }

        public StoryStatus StatusId { get; set; }
    }
}
