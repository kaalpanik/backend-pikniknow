﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravelApp.Model.ViewModel
{
    public class SMSViewModel
    {
        public string Status { get; set; }
        public string Details { get; set; }
        public string SessionId { get; set; }
        public string MobileNumber { get; set; }
        public string OTPCode { get; set; }
        public bool Flag { get; set; }
    }
}
