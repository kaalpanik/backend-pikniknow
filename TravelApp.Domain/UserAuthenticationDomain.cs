﻿using Rx.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TravelApp.Infrastructure.Security;
using TravelApp.Infrastructure.Uow;
using TravelApp.Infrastructure.Utilities;
using TravelApp.Model.DbModel;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Domain
{
    public class UserAuthenticationDomain : IUserAuthenticationDomain
    {
        public ServerSetting ServerSetting { get; set; }
        private IApplicationUtility ApplicationUtility { get; set; }
        private HashSet<string> ValidationMessages { get; set; }
        private IJwtTokenProvider JwtTokenProvider { get; set; }
        private ITravelAppUow TravelAppUow { get; set; }
        public ICommunication Communication { get; set; }
        private IPasswordHash PasswordHash { get; set; }

        public UserAuthenticationDomain(ServerSetting serverSetting, IPasswordHash passwordHash, ICommunication communication, ITravelAppUow travelAppUow, IJwtTokenProvider jwtTokenProvider, IApplicationUtility applicationUtility)
        {
            JwtTokenProvider = jwtTokenProvider;
            TravelAppUow = travelAppUow;
            ApplicationUtility = applicationUtility;
            ValidationMessages = new HashSet<string>();
            Communication = communication;
            this.PasswordHash = passwordHash;
            ServerSetting = serverSetting;
        }
        public UserAuthenticationViewModel PostLogin(UserCredentialViewModel userCredentialViewModel)
        {

            UserAuthenticationViewModel userAuthenticationViewModel = null;
            if (!userCredentialViewModel.IsAdmin)
            {
                var response = OtpVerification(userCredentialViewModel);
                if (response != null)
                {
                    User user = TravelAppUow.Repository<User>().SingleOrDefault(t => t.UserId == response.UserId && t.StatusId != (int)Status.Delete);
                    if (user != null)
                    {
                        List<ApplicationUserToken> applicationUserTokens = TravelAppUow.Repository<ApplicationUserToken>().FindBy(x => x.UserId == user.UserId).ToList();
                        foreach (ApplicationUserToken userToken in applicationUserTokens)
                        {
                            TravelAppUow.RegisterDeleted(userToken);
                        }
                        return GetAuthentication(user, userCredentialViewModel);
                    }
                }
                else
                {

                }

            }
            else
            {
                User user = this.TravelAppUow.Repository<User>().SingleOrDefault(t => t.EmailId == userCredentialViewModel.Email && t.StatusId != (int)Status.Delete);
                if (user != null)
                {
                    List<ApplicationUserToken> applicationUserTokens = TravelAppUow.Repository<ApplicationUserToken>().FindBy(x => x.UserId == user.UserId).ToList();
                    foreach (ApplicationUserToken userToken in applicationUserTokens)
                    {
                        TravelAppUow.RegisterDeleted(userToken);
                    }
                    TravelAppUow.RegisterDirty<User>(user);
                    return GetAuthentication(user, userCredentialViewModel);
                }
                return GetAuthentication(user, userCredentialViewModel);
            }
            return userAuthenticationViewModel;
        }

        public UserAuthenticationViewModel GetAuthentication(User user, UserCredentialViewModel userCredentialViewModel)
        {
            ApplicationUserToken applicationUserToken = JwtTokenProvider.WriteToken(user);
            applicationUserToken.PlatformId = userCredentialViewModel.PlatformId;
            applicationUserToken.AccessedPlatform = (userCredentialViewModel.PlatformId == (int)Platform.Web ? "Web" : "Mobile");
            applicationUserToken.BrowserName = userCredentialViewModel.BrowserName;
            applicationUserToken.AppVersion = userCredentialViewModel.AppVersion;
            applicationUserToken.DeviceName = userCredentialViewModel.DeviceName;
            applicationUserToken.DeviceOSVersion = userCredentialViewModel.DeviceOSVersion;
            applicationUserToken.DeviceID = userCredentialViewModel.DeviceID;
            applicationUserToken.FCMToken = userCredentialViewModel.FCMToken;
            int roleId = Convert.ToInt32(user.RoleId);
            Role role = TravelAppUow.Repository<Role>().FindBy(x => x.RoleId == user.RoleId).FirstOrDefault();

            UserAuthenticationViewModel userAuthentication = new UserAuthenticationViewModel
            {
                Token = applicationUserToken.JwtToken.ToString(),
                EmailId = user.EmailId,
                FullName = user.UserName,
                RoleId = roleId,
                RoleName = (role != null ? role.RoleName : ""),
                FailedLogin = false,
                UserId = user.UserId,
                IsProfileUpdated = !string.IsNullOrEmpty(user.UserName) ? true : false
            };
            TravelAppUow.RegisterNew(applicationUserToken);
            TravelAppUow.Commit();
            return userAuthentication;
        }

        public HashSet<string> PostLoginValidation(UserCredentialViewModel userCredentialViewModel)
        {
            if (!userCredentialViewModel.IsAdmin)
            {
                //User user = TravelAppUow.Repository<User>().SingleOrDefault(t => t.MobileNumber.Trim() == userCredentialViewModel.MobileNumber.Trim() && t.StatusId != (int)Status.Delete);
                //if (user != null)
                //{
                //    //var result = Communication.Verify(user.SessionId, userCredentialViewModel.Otp);
                //    //if (result.Result.Flag)
                //    //{
                //    //    if (result.Result.Status != "Success")
                //    //    {
                //    //        this.ValidationMessages.Add("Invalid otp code");
                //    //    }
                //    //}
                //    if (user.OtpCode != userCredentialViewModel.Otp)
                //    {
                //        this.ValidationMessages.Add("Invalid otp code");
                //    }
                //}
                //else
                //{
                //    this.ValidationMessages.Add("Invalid otp code");
                //}
            }
            else
            {
                User user = TravelAppUow.Repository<User>().SingleOrDefault(t => t.EmailId == userCredentialViewModel.Email && t.RoleId == (int)Roles.Admin && t.StatusId != (int)Status.Delete);
                if (user != null)
                {
                    if (user.StatusId == (int)Status.InActive)
                    {
                        this.ValidationMessages.Add("Account is not activated");
                    }
                    if (!(this.PasswordHash.VerifySignature(userCredentialViewModel.Password, user.Password, user.Salt)))
                    {
                        this.ValidationMessages.Add("Email or Password is Incorrect.");
                    }
                }
                else
                {
                    this.ValidationMessages.Add("Account not found");
                }
            }
            return this.ValidationMessages;
        }

        public OtpVerificationViewModel OtpVerification(UserCredentialViewModel userCredentialViewModel)
        {
            User user = null;
            bool isProfileSubmitted = false;
            if (!string.IsNullOrEmpty(userCredentialViewModel.Email))
            {
                user = TravelAppUow.Repository<User>().SingleOrDefault(t => t.EmailId.Trim() == userCredentialViewModel.Email.Trim() && t.StatusId != (int)Status.Delete);
            }
            else if (!string.IsNullOrEmpty(userCredentialViewModel.MobileNumber))
            {
                user = TravelAppUow.Repository<User>().SingleOrDefault(t => t.MobileNumber.Trim() == userCredentialViewModel.MobileNumber.Trim() && t.StatusId != (int)Status.Delete);
                isProfileSubmitted = true;
            }
            if (user != null)
            {
                if (!string.IsNullOrEmpty(userCredentialViewModel.Email))
                {
                    user.UserName = userCredentialViewModel.UserName;
                    user.EmailId = userCredentialViewModel.Email;
                    user.UId = userCredentialViewModel.UId;
                    user.IsProfileSubmitted = isProfileSubmitted;
                    user.OtpCode = this.ApplicationUtility.GenerateRandomOTP();
                    TravelAppUow.RegisterDirty<User>(user);
                    TravelAppUow.Commit();
                }
                return new OtpVerificationViewModel
                {
                    IsRegister = false,
                    UserId = user.UserId,
                    Otp = user.OtpCode
                };
            }
            else
            {
                user = new User()
                {
                    MobileNumber = userCredentialViewModel.MobileNumber.Trim(),
                    CreatedById = 1,
                    CreatedByDate = DateTime.UtcNow,
                    StatusId = (int)Status.Active,
                    RoleId = (int)Roles.User,
                    OtpCode = this.ApplicationUtility.GenerateRandomOTP(),
                    ProfilePicturePath = userCredentialViewModel.ProfilePicturePath,
                    UserName = userCredentialViewModel.UserName,
                    EmailId = userCredentialViewModel.Email,
                    UId = userCredentialViewModel.UId,
                    IsProfileSubmitted = isProfileSubmitted

                };
                TravelAppUow.RegisterNew<User>(user);
                TravelAppUow.Commit();
                return new OtpVerificationViewModel
                {
                    IsRegister = true,
                    UserId = user.UserId,
                    Otp = user.OtpCode
                };
            }
        }

        public HashSet<string> OtpVerificationValidation(UserCredentialViewModel userCredentialViewModel)
        {
            return ValidationMessages;
        }

        public void EmailVerification(UserCredentialViewModel userCredentialViewModel)
        {
            User user = TravelAppUow.Repository<User>().SingleOrDefault(t => t.EmailId == userCredentialViewModel.Email && t.RoleId == (int)Roles.Admin && t.StatusId != (int)Status.Delete);
            if (user != null)
            {
                user.VerificationCode = Guid.NewGuid();
                TravelAppUow.RegisterDirty(user);
                TravelAppUow.Commit();
                string emailIds = "lohar.devendra1994@gmail.com;jay@kaalpanik.in;";
                string emailBodyTemplate = string.Empty;
                emailIds += user.EmailId;
                emailBodyTemplate = EmailBody("ForgotPassword.html", user);
                Communication.SendSMTPMail(emailIds, ServerSetting.Get<string>("EmailTemplates.ForgotPassword.Subject"), emailBodyTemplate);
            }

        }
        public string EmailBody(string templateName, User user)
        {
            string body = string.Empty;
            Dictionary<string, object> keyProperties = null;
            string link = string.Empty;
            string siteName = ServerSetting.Get<string>("EmailTemplates.Settings.SiteUrl");
            link += "<a href='" + siteName + "/#/forgot-password?userId=" + user.UserId + "&verficationCode=" + user.VerificationCode + "+&emailId=" + user.EmailId + "'>Click here</a>";

            keyProperties = new Dictionary<string, object>
                     {
                         { "##userName##",!string.IsNullOrEmpty(user.UserName)? user.UserName:""  },
                         { "##link##",link },
                      };
            body = ApplicationUtility.GetEmailTemplate(templateName, keyProperties);
            return body;
        }
        public HashSet<string> EmailVerificationValidation(UserCredentialViewModel userCredentialViewModel)
        {
            User user = TravelAppUow.Repository<User>().SingleOrDefault(t => t.EmailId == userCredentialViewModel.Email && t.RoleId == (int)Roles.Admin && t.StatusId != (int)Status.Delete);
            if (user != null)
            {
                if (user.StatusId == (int)Status.InActive)
                {
                    this.ValidationMessages.Add("Account is not activated");
                }
            }
            else
            {
                this.ValidationMessages.Add("Account not found");
            }
            return ValidationMessages;
        }

        public void ResetPassword(UserCredentialViewModel userCredentialViewModel)
        {
            User user = this.TravelAppUow.Repository<User>().SingleOrDefault(t => t.UserId == userCredentialViewModel.UserId && t.EmailId == userCredentialViewModel.Email && t.StatusId != (int)Status.Delete);
            if (user != null)
            {
                var data = PasswordHash.Encrypt(userCredentialViewModel.ConfirmPassword);
                user.Password = data.Signature;
                user.Salt = data.Salt;
                TravelAppUow.RegisterDirty(user);
                TravelAppUow.Commit();
            }
        }

        public HashSet<string> ResetPasswordValidation(UserCredentialViewModel userCredentialViewModel)
        {
            if (!userCredentialViewModel.VerificationCode.HasValue || userCredentialViewModel.VerificationCode == Guid.Empty)
            {
                ValidationMessages.Add("Verification code is incorrect");
            }
            else if (userCredentialViewModel.Password != userCredentialViewModel.ConfirmPassword)
            {
                ValidationMessages.Add("Password and confrim password is not matched");
            }
            else
            {
                User user = this.TravelAppUow.Repository<User>().SingleOrDefault(t => t.UserId == userCredentialViewModel.UserId && t.EmailId == userCredentialViewModel.Email && t.StatusId != (int)Status.Delete);
                if (user != null)
                {
                    if (user.StatusId == (int)Status.InActive)
                    {
                        ValidationMessages.Add("Account is not active");
                    }
                    else
                    {
                        if (!(user.VerificationCode.HasValue && userCredentialViewModel.VerificationCode == user.VerificationCode))
                        {
                            ValidationMessages.Add("Verification code is incorrect");
                        }
                    }
                }
                else
                {
                    ValidationMessages.Add("User not found");
                }
            }
            return ValidationMessages;
        }
    }
    public interface IUserAuthenticationDomain
    {
        UserAuthenticationViewModel PostLogin(UserCredentialViewModel userCredentialViewModel);
        HashSet<string> PostLoginValidation(UserCredentialViewModel userCredentialViewModel);

        OtpVerificationViewModel OtpVerification(UserCredentialViewModel userCredentialViewModel);
        HashSet<string> OtpVerificationValidation(UserCredentialViewModel userCredentialViewModel);

        void EmailVerification(UserCredentialViewModel userCredentialViewModel);
        HashSet<string> EmailVerificationValidation(UserCredentialViewModel userCredentialViewModel);

        void ResetPassword(UserCredentialViewModel userCredentialViewModel);
        HashSet<string> ResetPasswordValidation(UserCredentialViewModel userCredentialViewModel);

    }
}
