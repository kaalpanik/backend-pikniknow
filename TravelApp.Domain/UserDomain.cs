﻿using Microsoft.AspNetCore.SignalR;
using Rx.Core.Data;
using Rx.Core.Security;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using TravelApp.Infrastructure.Context;
using TravelApp.Infrastructure.Uow;
using TravelApp.Infrastructure.Utilities;
using TravelApp.Model.DbModel;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Domain
{
    public class UserDomain : IUserDomain
    {
        private IHubContext<WebSocketEventHub> HubContext { get; set; }
        private IApplicationUtility ApplicationUtility { get; set; }
        private HashSet<string> ValidationMessages { get; set; }
        private IPasswordHash PasswordHash { get; set; }
        private ITravelAppUow TravelAppUow { get; set; }
        public IFileOperations FileOperations { get; set; }
        IDbContextManager<MainSqlDbContext> DbContextManager { get; set; }
        public UserDomain(IHubContext<WebSocketEventHub> hubContext, IPasswordHash passwordHash, IDbContextManager<MainSqlDbContext> dbContextManager, IFileOperations fileOperations, ITravelAppUow travelAppUow, IApplicationUtility applicationUtility)
        {
            TravelAppUow = travelAppUow;
            ApplicationUtility = applicationUtility;
            ValidationMessages = new HashSet<string>();
            FileOperations = fileOperations;
            DbContextManager = dbContextManager;
            this.PasswordHash = passwordHash;
            HubContext = hubContext;
        }
        public User Add(User user)
        {
            var data = PasswordHash.Encrypt("Admin@123");
            user.Password = data.Signature;
            user.Salt = data.Salt;
            TravelAppUow.RegisterNew<User>(user);
            TravelAppUow.Commit();
            return user;
        }

        public HashSet<string> AddValidation(User user)
        {
            bool isExistEmail = this.TravelAppUow.Repository<User>().FindBy(t => t.EmailId.ToLower().Trim() == user.EmailId.ToLower().Trim() && t.StatusId != (int)Status.Delete).Any();
            if (isExistEmail)
            {
                this.ValidationMessages.Add("Email id already exist");
            }
            bool isExistMobile = this.TravelAppUow.Repository<User>().FindBy(t => t.MobileNumber.ToLower().Trim() == user.MobileNumber.ToLower().Trim() && t.StatusId != (int)Status.Delete).Any();
            if (isExistMobile)
            {
                this.ValidationMessages.Add("Mobile number already exist");
            }
            return this.ValidationMessages;
        }

        public void Delete(int id)
        {
            var user = TravelAppUow.Repository<User>().SingleOrDefault(t => t.UserId == id);
            if (user != null)
            {
                user.StatusId = (int)Status.Delete;
                user.ModifiedByDate = DateTime.UtcNow;
                user.ModifiedById = UserClaim.UserId;
                TravelAppUow.RegisterDirty<User>(user);
                TravelAppUow.Commit();
            }
        }

        public HashSet<string> DeleteValidation(int id)
        {
            return this.ValidationMessages;
        }

        public string Get(string orderByColumn, string sortOrder, int pageIndex, int rowCount, string searchQuery, int userId, int roleId)
        {
            var spParameters = new object[7];
            spParameters[0] = new SqlParameter() { ParameterName = "UserId", Value = userId };
            spParameters[1] = new SqlParameter() { ParameterName = "SearchQuery", Value = string.IsNullOrEmpty(searchQuery) ? "" : searchQuery };
            spParameters[2] = new SqlParameter() { ParameterName = "OrderByColumn", Value = string.IsNullOrEmpty(orderByColumn) ? "" : orderByColumn };
            spParameters[3] = new SqlParameter() { ParameterName = "SortOrder", Value = string.IsNullOrEmpty(sortOrder) ? "" : sortOrder };
            spParameters[4] = new SqlParameter() { ParameterName = "PageIndex", Value = pageIndex };
            spParameters[5] = new SqlParameter() { ParameterName = "RowCount", Value = rowCount };
            spParameters[6] = new SqlParameter() { ParameterName = "RoleId", Value = roleId };
            var storeProcSearchResult = DbContextManager.SqlQueryAsync<StoreProcSearchViewModel>("EXEC dbo.spUsers @UserId,@SearchQuery,@OrderByColumn,@SortOrder,@PageIndex,@RowCount,@RoleId", spParameters).Result;
            return storeProcSearchResult.SingleOrDefault()?.Result;
        }

        public User Get(int id)
        {
            var user = TravelAppUow.Repository<User>().SingleOrDefault(t => t.UserId == id);
            return user;
        }

        public User Update(User user)
        {
            if (user.IsChangePassword)
            {
                User isUserExist = TravelAppUow.Repository<User>().SingleOrDefault(t => t.UserId == user.UserId && t.RoleId == (int)Roles.Admin && t.StatusId != (int)Status.Delete);
                if (isUserExist != null)
                {
                    var passwordHash = this.PasswordHash.Encrypt(user.NewPassword);
                    isUserExist.Password = passwordHash.Signature;
                    isUserExist.Salt = passwordHash.Salt;
                    TravelAppUow.RegisterDirty<User>(isUserExist);
                    TravelAppUow.Commit();
                    return user;
                }
            }
            else
            {
                if (user.ProfileMultipartImage != null)
                {
                    if (user.ProfileMultipartImage.Length > 0)
                    {
                        using (var ms = new MemoryStream())
                        {
                            user.ProfileMultipartImage.CopyTo(ms);
                            var fileBytes = ms.ToArray();
                            string base64Images = Convert.ToBase64String(fileBytes);
                            if (!string.IsNullOrEmpty(base64Images))
                            {
                                string extension = Path.GetExtension(user.ProfileMultipartImage.FileName);
                                var fileName = "Profile_Picture" + Guid.NewGuid().ToString() + user.ProfileMultipartImage.FileName;
                                user.ProfilePicturePath = "https://travel-app-media.s3.ap-south-1.amazonaws.com/"+fileName;
                                FileOperations.UploadFileToS3(user.ProfileMultipartImage, fileName); // TODO
                                var result = new
                                {
                                    user.ProfilePicturePath,
                                    user.UserId,
                                };
                                var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                                HubContext.Clients.All.SendAsync("userDetails", result);
                            }
                        }
                    }
                }
                user.ModifiedByDate = DateTime.UtcNow;
                user.ModifiedById = 1;
                TravelAppUow.RegisterDirty<User>(user);
                TravelAppUow.Commit();
            }
            return user;
        }

        public HashSet<string> UpdateValidation(User user)
        {
            if (user.IsChangePassword)
            {
                User isUserExist = TravelAppUow.Repository<User>().SingleOrDefault(t => t.UserId == user.UserId && t.RoleId == (int)Roles.Admin && t.StatusId != (int)Status.Delete);
                if (isUserExist != null)
                {
                    if (!(this.PasswordHash.VerifySignature(user.OldPassword, user?.Password, user?.Salt)))
                    {
                        this.ValidationMessages.Add("Incorrect Old Password");
                    }
                    if (user.NewPassword != user.ConfirmPassword)
                    {
                        this.ValidationMessages.Add("Confirm Password is not matched");
                    }
                }
                else
                {
                    this.ValidationMessages.Add("User not found");
                }
               
            }
            else
            {
                if (!string.IsNullOrEmpty(user.EmailId))
                {
                    var isEmailExists = this.TravelAppUow.Repository<User>().SingleOrDefault(t => t.UserId != user.UserId && t.EmailId.ToLower().Trim() == user.EmailId.ToLower().Trim() && t.StatusId != (int)Status.Delete);
                    if (isEmailExists != null)
                    {
                        ValidationMessages.Add("Email ID already exist.");
                    }
                }
                if (!string.IsNullOrEmpty(user.MobileNumber))
                {
                    var isMobileNumberExists = this.TravelAppUow.Repository<User>().SingleOrDefault(t => t.UserId != user.UserId && t.MobileNumber.ToLower().Trim() == user.MobileNumber.ToLower().Trim() && t.StatusId != (int)Status.Delete);
                    if (isMobileNumberExists != null)
                    {
                        ValidationMessages.Add("Mobile Number already exist.");
                    }
                }
                

            }
            return this.ValidationMessages;
        }

        public string UserNotification(int userId, int type, int pageIndex, int rowCount)
        {
            var spParameters = new object[4];
            spParameters[0] = new SqlParameter() { ParameterName = "UserId", Value = userId };
            spParameters[1] = new SqlParameter() { ParameterName = "Type", Value = type };
            spParameters[2] = new SqlParameter() { ParameterName = "PageIndex", Value = pageIndex };
            spParameters[3] = new SqlParameter() { ParameterName = "RowCount", Value = rowCount };
            var storeProcSearchResult = DbContextManager.SqlQueryAsync<StoreProcSearchViewModel>("EXEC spUserNotifications @UserId,@Type,@PageIndex,@RowCount", spParameters).Result;
            return storeProcSearchResult.SingleOrDefault()?.Result;
        }
    }
    public interface IUserDomain
    {
        string Get(string orderByColumn, string sortOrder, int pageIndex, int rowCount, string searchQuery, int userId, int roleId);
        User Get(int id);

        HashSet<string> AddValidation(User user);
        HashSet<string> UpdateValidation(User user);

        User Add(User user);
        User Update(User user);
        HashSet<string> DeleteValidation(int id);
        void Delete(int id);

        string UserNotification(int userPoolId, int type, int pageIndex, int rowCount);

    }
}
