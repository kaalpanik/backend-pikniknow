﻿using Microsoft.AspNetCore.SignalR;
using Rx.Core.Data;
using Rx.Core.Security;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using TravelApp.Infrastructure.Context;
using TravelApp.Infrastructure.Uow;
using TravelApp.Infrastructure.Utilities;
using TravelApp.Model.DbModel;
using TravelApp.Model.SystemEnums;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;


namespace TravelApp.Domain
{
    public class UserFollowerDomain : IUserFollowerDomain
    {
        private IHubContext<WebSocketEventHub> HubContext { get; set; }
        private ITravelAppUow TravelAppUow { get; set; }
        public ICommunication Communication { get; set; }
        IDbContextManager<MainSqlDbContext> DbContextManager { get; set; }
        public UserFollowerDomain(ICommunication communication,IHubContext<WebSocketEventHub> hubContext, IDbContextManager<MainSqlDbContext> dbContextManager, ITravelAppUow travelAppUow)
        {
            TravelAppUow = travelAppUow;
            DbContextManager = dbContextManager;
            HubContext = hubContext;
            Communication = communication;
        }
        public string Get(int userId)
        {
            return "";
        }

        public UserFollowerViewModel Operation(UserFollowerViewModel userFollowerViewModel)
        {
            if(userFollowerViewModel.UserFollowerTypeId == TravelAppEnums.UserFollowerType.Following)
            {
               
            }
            if (userFollowerViewModel.UserFollowerTypeId == TravelAppEnums.UserFollowerType.Follow)
            {
                if (!userFollowerViewModel.UnFollow)
                {
                    UserFollower userFollower = new UserFollower()
                    {
                        CreatedByDate = DateTime.Now,
                        CreatedById = userFollowerViewModel.CreatedById,
                        UserFollowerFriendId = userFollowerViewModel.UserFollowerFriendId,
                        TypeId = (int)TravelAppEnums.UserFollowerType.Follow,
                        StatusId = (int)TravelAppEnums.Status.Active,
                    };
                    TravelAppUow.RegisterNew(userFollower);
                    TravelAppUow.Commit();
                    var result = new
                    {
                        FollowUserId = userFollowerViewModel.CreatedById,
                        FollowingUserId = userFollowerViewModel.UserFollowerFriendId,
                        following = true
                    };
                    var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                    HubContext.Clients.All.SendAsync("userFollow", result);
                    vUser vUser = TravelAppUow.Repository<vUser>().FindBy(x => x.UserId == userFollowerViewModel.CreatedById).FirstOrDefault();
                    if (vUser != null)
                    {
                        string body = vUser.UserName + " Start following you";
                        List<vApplicationUserToken> vApplicationUserToken = TravelAppUow.Repository<vApplicationUserToken>().FindBy(x => x.UserId == userFollowerViewModel.UserFollowerFriendId).ToList();
                        if (vApplicationUserToken.Count() > 0)
                        {
                            Communication.SendNotification(userFollowerViewModel.UserFollowerFriendId,NotificationType.StoryComment, "Story Follow", body, vApplicationUserToken);
                        }
                    }
                }
                else
                {
                    var unFollowUser = TravelAppUow.Repository<UserFollower>().FindBy(x => x.CreatedById == userFollowerViewModel.CreatedById && x.UserFollowerFriendId == userFollowerViewModel.UserFollowerFriendId).FirstOrDefault();
                    if (unFollowUser != null)
                    {
                        TravelAppUow.RegisterDeleted(unFollowUser);
                        TravelAppUow.Commit();
                    }
                    var result = new
                    {
                        FollowUserId = userFollowerViewModel.CreatedById,
                        FollowingUserId = userFollowerViewModel.UserFollowerFriendId,
                        following = false
                    };
                    var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                    HubContext.Clients.All.SendAsync("userUnFollow", result);
                }
            }
            return userFollowerViewModel;
        }
        public string Get(string searchQuery, string orderByColumn, string sortOrder, int pageIndex, int rowCount, int userId, int typeId)
        {
            var spParameters = new object[7];
            spParameters[0] = new SqlParameter() { ParameterName = "SearchQuery", Value = string.IsNullOrEmpty(searchQuery) ? "" : searchQuery };
            spParameters[1] = new SqlParameter() { ParameterName = "OrderByColumn", Value = string.IsNullOrEmpty(orderByColumn) ? "" : orderByColumn };
            spParameters[2] = new SqlParameter() { ParameterName = "SortOrder", Value = string.IsNullOrEmpty(sortOrder) ? "" : sortOrder };
            spParameters[3] = new SqlParameter() { ParameterName = "PageIndex", Value = pageIndex };
            spParameters[4] = new SqlParameter() { ParameterName = "RowCount", Value = rowCount };
            spParameters[5] = new SqlParameter() { ParameterName = "UserId", Value = userId };
            spParameters[6] = new SqlParameter() { ParameterName = "TypeId", Value = typeId };
            var storeProcSearchResult = DbContextManager.SqlQueryAsync<StoreProcSearchViewModel>("EXEC dbo.spUserFollowers @SearchQuery,@OrderByColumn,@SortOrder,@PageIndex,@RowCount,@UserId,@TypeId", spParameters).Result;
            return storeProcSearchResult.SingleOrDefault()?.Result;
        }
    }
    public interface IUserFollowerDomain
    {
        string Get(int userId);
        string Get(string searchQuery, string orderByColumn, string sortOrder, int pageIndex, int rowCount, int userId,int typeId);
        UserFollowerViewModel Operation(UserFollowerViewModel userFollowerViewModel);
    }
}
