﻿using Rx.Core.Data;
using Rx.Core.Security;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using TravelApp.Infrastructure.Context;
using TravelApp.Infrastructure.Uow;
using TravelApp.Infrastructure.Utilities;
using TravelApp.Model.DbModel;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Domain
{
    public class BannerDomain : IBannerDomain
    {
        private ITravelAppUow TravelAppUow { get; set; }
        public IFileOperations FileOperations { get; set; }
        IDbContextManager<MainSqlDbContext> DbContextManager { get; set; }
        private IApplicationUtility ApplicationUtility { get; set; }
        private HashSet<string> ValidationMessages { get; set; }
        public BannerDomain(IDbContextManager<MainSqlDbContext> dbContextManager, IFileOperations fileOperations, ITravelAppUow travelAppUow, IApplicationUtility applicationUtility)
        {
            TravelAppUow = travelAppUow;
            ApplicationUtility = applicationUtility;
            ValidationMessages = new HashSet<string>();
            FileOperations = fileOperations;
            DbContextManager = dbContextManager;
        }

        public Banner Add(Banner banner)
        {
            if (banner.BannerImage != null)
            {
                if (banner.BannerImage.Length > 0)
                {
                    using (var ms = new MemoryStream())
                    {
                        banner.BannerImage.CopyTo(ms);
                        var fileBytes = ms.ToArray();
                        string base64Images = Convert.ToBase64String(fileBytes);
                        if (!string.IsNullOrEmpty(base64Images))
                        {
                            string extension = Path.GetExtension(banner.BannerImage.FileName);
                            var fileName = "Banner" + Guid.NewGuid().ToString() + banner.BannerImage.FileName;
                            banner.ImageURL = fileName;
                            FileOperations.UploadFileToS3(banner.BannerImage, fileName);
                        }
                    }
                }
            }
            TravelAppUow.RegisterNew<Banner>(banner);
            TravelAppUow.Commit();
            return banner;
        }

        public HashSet<string> AddValidation(Banner banner)
        {
            return ValidationMessages;
        }

        public void Delete(int id)
        {
            var banner = TravelAppUow.Repository<Banner>()
                                        .FindBy(x => x.BannerId == id).FirstOrDefault();
            if (banner != null)
            {
                TravelAppUow.RegisterDeleted<Banner>(banner);
                TravelAppUow.Commit();
            }
        }

        public HashSet<string> DeleteValidation(int id)
        {
            return ValidationMessages;
        }

        public IEnumerable<Banner> Get()
        {
            var banner = TravelAppUow.Repository<Banner>().All().AsQueryable().OrderBy(x=>x.OrderId);
            return banner;
        }

        public Banner Get(int id)
        {
            var banner = TravelAppUow.Repository<Banner>()
                                          .FindBy(x => x.BannerId == id).FirstOrDefault();
            return banner;
        }

        public Banner Update(Banner banner)
        {
            if (banner.BannerImage != null)
            {
                if (banner.BannerImage.Length > 0)
                {
                    using (var ms = new MemoryStream())
                    {
                        banner.BannerImage.CopyTo(ms);
                        var fileBytes = ms.ToArray();
                        string base64Images = Convert.ToBase64String(fileBytes);
                        if (!string.IsNullOrEmpty(base64Images))
                        {
                            string extension = Path.GetExtension(banner.BannerImage.FileName);
                            var fileName = "Banner" + Guid.NewGuid().ToString() + banner.BannerImage.FileName;
                            banner.ImageURL = fileName;
                            FileOperations.UploadFileToS3(banner.BannerImage, fileName);
                        }
                    }
                }
            }
            TravelAppUow.RegisterDirty<Banner>(banner);
            TravelAppUow.Commit();
            return banner;
        }

        public HashSet<string> UpdateValidation(Banner banner)
        {
            return ValidationMessages;
        }
    }
    public interface IBannerDomain
    {
        IEnumerable<Banner> Get();
        Banner Get(int id);

        HashSet<string> AddValidation(Banner banner);
        HashSet<string> UpdateValidation(Banner banner);

        Banner Add(Banner banner);
        Banner Update(Banner banner);
        HashSet<string> DeleteValidation(int id);
        void Delete(int id);
    }
}
