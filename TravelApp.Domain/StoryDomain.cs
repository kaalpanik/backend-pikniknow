﻿using Microsoft.AspNetCore.SignalR;
using Rx.Core.Data;
using Rx.Core.Security;
using Rx.Core.Settings;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using TravelApp.Infrastructure.Context;
using TravelApp.Infrastructure.Uow;
using TravelApp.Infrastructure.Utilities;
using TravelApp.Model.DbModel;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Domain
{
    public class StoryDomain : IStoryDomain
    {
        private IHubContext<WebSocketEventHub> HubContext { get; set; }
        private IApplicationUtility ApplicationUtility { get; set; }
        private HashSet<string> ValidationMessages { get; set; }
        public ICommunication Communication { get; set; }
        private ITravelAppUow TravelAppUow { get; set; }
        public IFileOperations FileOperations { get; set; }
        IDbContextManager<MainSqlDbContext> DbContextManager { get; set; }
        public ServerSetting ServerSetting { get; set; }
        public StoryDomain(ServerSetting serverSetting, ICommunication communication, IHubContext<WebSocketEventHub> hubContext, IDbContextManager<MainSqlDbContext> dbContextManager, IFileOperations fileOperations, ITravelAppUow travelAppUow, IApplicationUtility applicationUtility)
        {
            TravelAppUow = travelAppUow;
            ApplicationUtility = applicationUtility;
            ValidationMessages = new HashSet<string>();
            FileOperations = fileOperations;
            DbContextManager = dbContextManager;
            HubContext = hubContext;
            Communication = communication;
            ServerSetting = serverSetting;
        }
        public Story Add(Story story)
        {
            int i = 0;
            story.StatusId = (int)StoryStatus.Pending;
            story.CreatedByDate = DateTime.Now;
            foreach (var item in story.StoryMedias)
            {
                item.FilePath = "temp";
                if (item.StoryMediaPath != null)
                {
                    if (item.StoryMediaPath.Length > 0)
                    {
                        item.IsPrimaryMedia = i == 0 ? true : false;
                        using (var ms = new MemoryStream())
                        {
                            item.StoryMediaPath.CopyTo(ms);
                            var fileBytes = ms.ToArray();
                            string base64Images = Convert.ToBase64String(fileBytes);
                            if (!string.IsNullOrEmpty(base64Images))
                            {
                                string extension = Path.GetExtension(item.StoryMediaPath.FileName);
                                var fileName = "StoryMedia" + Guid.NewGuid().ToString() + item.StoryMediaPath.FileName;
                                item.FilePath = fileName;
                                FileOperations.UploadFileToS3(item.StoryMediaPath, fileName);
                            }
                        }
                        i++;
                    }
                }
            }
            foreach (var item in story.StoryTags)
            {
                if (item.TagId == 0)
                {
                    var tag = TravelAppUow.Repository<Tag>().FindBy(x => x.Name.ToLower().Trim() == item.Name.ToLower().Trim()).FirstOrDefault();
                    if (tag == null)
                    {
                        tag = new Tag()
                        {
                            Name = item.Name,
                            StatusId = (int)Status.Active
                        };
                        TravelAppUow.RegisterNew<Tag>(tag);
                        TravelAppUow.Commit();
                        item.TagId = tag.TagId;
                    }
                    else
                    {
                        item.TagId = tag.TagId;
                    }

                }
            }
            TravelAppUow.RegisterNew<Story>(story);
            TravelAppUow.Commit();

            vStories vStories = TravelAppUow.Repository<vStories>().FindBy(x => x.StoryId == story.StoryId).FirstOrDefault();
            if (vStories != null)
            {
                var vUser = TravelAppUow.Repository<vUser>().FindBy(x => x.RoleId == 1).Select(x => new { x.EmailId }).ToList();
                if (vUser.Count() > 0)
                {
                    string emailIds = "lohar.devendra1994@gmail.com;jay@kaalpanik.in;";
                    foreach (var item in vUser)
                    {
                        emailIds += item.EmailId + ";";
                    }
                    string emailBodyTemplate = string.Empty;
                    emailBodyTemplate = EmailBody("new-story.html", vStories);
                    Communication.SendSMTPMail(emailIds, ServerSetting.Get<string>("EmailTemplates.NewStorySubmit.Subject"), emailBodyTemplate);
                }
            }
            return story;
        }

        public HashSet<string> AddValidation(Story story)
        {
            return this.ValidationMessages;
        }

        public void Delete(int id)
        {

        }

        public HashSet<string> DeleteValidation(int id)
        {
            return this.ValidationMessages;
        }

        public string Get(int userId, string searchQuery, string orderByColumn, string sortOrder, int pageIndex, int rowCount, int storyId)
        {
            var spParameters = new object[7];
            spParameters[0] = new SqlParameter() { ParameterName = "UserId", Value = userId };
            spParameters[1] = new SqlParameter() { ParameterName = "SearchQuery", Value = string.IsNullOrEmpty(searchQuery) ? "" : searchQuery };
            spParameters[2] = new SqlParameter() { ParameterName = "OrderByColumn", Value = string.IsNullOrEmpty(orderByColumn) ? "" : orderByColumn };
            spParameters[3] = new SqlParameter() { ParameterName = "SortOrder", Value = string.IsNullOrEmpty(sortOrder) ? "" : sortOrder };
            spParameters[4] = new SqlParameter() { ParameterName = "PageIndex", Value = pageIndex };
            spParameters[5] = new SqlParameter() { ParameterName = "RowCount", Value = rowCount };
            spParameters[6] = new SqlParameter() { ParameterName = "StoryId", Value = storyId };
            var storeProcSearchResult = DbContextManager.SqlQueryAsync<StoreProcSearchViewModel>("EXEC dbo.spStories @UserId,@SearchQuery,@OrderByColumn,@SortOrder,@PageIndex,@RowCount,@StoryId", spParameters).Result;
            return storeProcSearchResult.SingleOrDefault()?.Result;
        }
        public Story Get(int id)
        {
            var story = TravelAppUow.Repository<Story>()
                                        .FindByInclude(x => x.StoryId == id,
                                        x => x.StoryTags,
                                        x => x.StoryMedias
                                        )
                                        .SingleOrDefault();

            return story;
        }

        public Story Update(Story story)
        {
            int i = 0;
            foreach (var item in story.StoryMedias)
            {
                if (item.StoryMediaPath != null && item.StoryMediaId > 0)
                {
                    if (item.StoryMediaPath.Length > 0)
                    {
                        //item.IsPrimaryMedia = i == 0 ? true : false;
                        using (var ms = new MemoryStream())
                        {
                            item.StoryMediaPath.CopyTo(ms);
                            var fileBytes = ms.ToArray();
                            string base64Images = Convert.ToBase64String(fileBytes);
                            if (!string.IsNullOrEmpty(base64Images))
                            {
                                string extension = Path.GetExtension(item.StoryMediaPath.FileName);
                                var fileName = "StoryMedia" + story.CreatedById + "TimeStamP_" + DateTime.Now.ToString("MMddyyyyHHMMss") + item.StoryMediaPath.FileName;
                                var fileDetail = new FileDetailModel
                                {
                                    FileName = fileName,
                                    FileData = base64Images,
                                };
                                item.FilePath = FileOperations.SaveFile(fileDetail, ""); // TODO
                            }
                        }
                        i++;
                    }
                }
                if (item.StatusId == (int)Status.Delete)
                {
                    TravelAppUow.RegisterDeleted<StoryMedia>(item);
                }
                if (item.StoryMediaId == 0)
                {
                    item.FilePath = "temp";
                    if (item.StoryMediaPath != null)
                    {
                        if (item.StoryMediaPath.Length > 0)
                        {
                            using (var ms = new MemoryStream())
                            {
                                item.StoryMediaPath.CopyTo(ms);
                                var fileBytes = ms.ToArray();
                                string base64Images = Convert.ToBase64String(fileBytes);
                                if (!string.IsNullOrEmpty(base64Images))
                                {
                                    string extension = Path.GetExtension(item.StoryMediaPath.FileName);
                                    var fileName = "StoryMedia" + Guid.NewGuid().ToString() + item.StoryMediaPath.FileName;
                                    item.FilePath = fileName;
                                    FileOperations.UploadFileToS3(item.StoryMediaPath, fileName);
                                }
                            }
                            i++;
                        }
                    }
                    TravelAppUow.RegisterNew<StoryMedia>(item);
                }
            }
            foreach (var item in story.StoryTags)
            {
                if (item.StatusId == (int)Status.Delete)
                {
                    TravelAppUow.RegisterDeleted<StoryTag>(item);
                }
                else if (item.TagId == 0)
                {
                    var tag = TravelAppUow.Repository<Tag>().FindBy(x => x.Name.ToLower().Trim() == item.Name.ToLower().Trim()).FirstOrDefault();
                    if (tag == null)
                    {
                        tag = new Tag()
                        {
                            Name = item.Name,
                            StatusId = (int)Status.Active
                        };
                        TravelAppUow.RegisterNew<Tag>(tag);
                        TravelAppUow.Commit();
                        item.TagId = tag.TagId;
                    }
                    else
                    {
                        item.TagId = tag.TagId;
                    }

                }
            }
            TravelAppUow.RegisterDirty<Story>(story);
            TravelAppUow.Commit();
            return story;
        }

        public HashSet<string> UpdateValidation(Story story)
        {
            return this.ValidationMessages;
        }

        public StoryViewModel ManageStory(StoryViewModel storyViewModel)
        {
            //vStories vStories1 = TravelAppUow.Repository<vStories>().FindBy(x => x.StoryId == storyViewModel.StoryId).FirstOrDefault();
            //if (vStories1 != null)
            //{
            //    var vUser = TravelAppUow.Repository<vUser>().FindBy(x => x.RoleId == 1).Select(x => new { x.EmailId }).ToList();
            //    if (vUser.Count() > 0)
            //    {
            //        string emailIds = "lohar.devendra1994@gmail.com;jay@kaalpanik.in;";
            //        foreach (var item in vUser)
            //        {
            //            emailIds += item.EmailId + ";";
            //        }
            //        string emailBodyTemplate = string.Empty;
            //        emailBodyTemplate = EmailBody("new-story.html", vStories1);
            //        Communication.SendSMTPMail(emailIds, ServerSetting.Get<string>("EmailTemplates.NewStorySubmit.Subject"), emailBodyTemplate);
            //    }
            //}
            if (storyViewModel.OperationType == OperationType.Like)
            {
                var isAlreadyLike = TravelAppUow.Repository<UserStory>()
                                      .FindBy(x => x.StoryId == storyViewModel.StoryId && x.UserId == storyViewModel.UserId && x.TypeId == OperationType.Like).FirstOrDefault();
                if (isAlreadyLike != null && !storyViewModel.IsLike)
                {
                    TravelAppUow.RegisterDeleted<UserStory>(isAlreadyLike);
                    TravelAppUow.Commit();
                }
                else
                {
                    if (isAlreadyLike == null)
                    {
                        isAlreadyLike = new UserStory()
                        {
                            UserId = storyViewModel.UserId,
                            StoryId = storyViewModel.StoryId,
                            TypeId = OperationType.Like
                        };
                        TravelAppUow.RegisterNew<UserStory>(isAlreadyLike);
                        TravelAppUow.Commit();
                        vUser vUser = TravelAppUow.Repository<vUser>().FindBy(x => x.UserId == storyViewModel.UserId).FirstOrDefault();
                        vStories vStories = TravelAppUow.Repository<vStories>().FindBy(x => x.StoryId == storyViewModel.StoryId).FirstOrDefault();
                        if (vUser != null && vStories != null)
                        {
                            string body = vUser.UserName + " Likes your story " + vStories.Title;
                            List<vApplicationUserToken> vApplicationUserToken = TravelAppUow.Repository<vApplicationUserToken>().FindBy(x => x.UserId == vStories.CreatedById).ToList();
                            if (vApplicationUserToken.Count() > 0)
                            {
                                Communication.SendNotification(vStories.CreatedById, NotificationType.Like, "Story Like", body, vApplicationUserToken);
                            }
                        }
                    }


                }
                var totalLike = TravelAppUow.Repository<UserStory>()
                                      .FindBy(x => x.StoryId == storyViewModel.StoryId && x.TypeId == OperationType.Like).Count();
                var result = new
                {
                    storyViewModel.IsLike,
                    storyViewModel.StoryId,
                    storyViewModel.UserId,
                    totalLike
                };
                var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                HubContext.Clients.All.SendAsync("storyLikes", result);

            }
            if (storyViewModel.OperationType == OperationType.View)
            {
                var isAlreadyView = TravelAppUow.Repository<UserStory>()
                                     .FindBy(x => x.StoryId == storyViewModel.StoryId && x.UserId == storyViewModel.UserId && x.TypeId == OperationType.View).FirstOrDefault();
                if (isAlreadyView == null)
                {
                    var isAlreadyLike = new UserStory()
                    {
                        UserId = storyViewModel.UserId,
                        StoryId = storyViewModel.StoryId,
                        TypeId = OperationType.View
                    };
                    TravelAppUow.RegisterNew<UserStory>(isAlreadyLike);
                    TravelAppUow.Commit();
                    int totalView = 0;
                    totalView = TravelAppUow.Repository<UserStory>()
                                    .FindBy(x => x.StoryId == storyViewModel.StoryId && x.TypeId == OperationType.View).Count();
                    var result = new
                    {
                        storyViewModel.StoryId,
                        totalView
                    };
                    var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                    HubContext.Clients.All.SendAsync("storyView", result);
                }
            }
            if (storyViewModel.OperationType == OperationType.Share)
            {
                var story = TravelAppUow.Repository<Story>()
                                      .FindBy(x => x.StoryId == storyViewModel.StoryId).SingleOrDefault();
                if (story != null)
                {
                    story.TotalShare += 1;
                    TravelAppUow.RegisterDirty<Story>(story);
                    TravelAppUow.Commit();
                }
            }
            return storyViewModel;
        }

        public StoryViewModel UpdateStory(StoryViewModel storyViewModel)
        {
            Story story = this.Get(storyViewModel.StoryId);
            if (story != null)
            {
                story.StatusId = (int)storyViewModel.StatusId;
                TravelAppUow.RegisterDirty(story);
                TravelAppUow.Commit();
                vStories vStories = TravelAppUow.Repository<vStories>().FindBy(x => x.StoryId == storyViewModel.StoryId).FirstOrDefault();
                if (vStories != null)
                {
                    string body = string.Empty;
                    string title = string.Empty;
                    if (story.StatusId == 9)
                    {
                        body = "Your " + vStories.Title + " story Approved By Admin";
                        title = "Story Approved";
                    }
                    else if (story.StatusId == 10)
                    {
                        body = "Your " + vStories.Title + " story Rejected By Admin";
                        title = "Story Rejected";
                    }
                    List<vApplicationUserToken> vApplicationUserToken = TravelAppUow.Repository<vApplicationUserToken>().FindBy(x => x.UserId == vStories.CreatedById).ToList();
                    if (vApplicationUserToken.Count() > 0)
                    {
                        Communication.SendNotification(vStories.CreatedById, NotificationType.Like, title, body, vApplicationUserToken);
                    }
                }
            }
            return storyViewModel;
        }

        public string GetLikeUserStory(int userId, string orderByColumn, string sortOrder, int pageIndex, int rowCount)
        {
            var spParameters = new object[5];
            spParameters[0] = new SqlParameter() { ParameterName = "UserId", Value = userId };
            spParameters[1] = new SqlParameter() { ParameterName = "OrderByColumn", Value = string.IsNullOrEmpty(orderByColumn) ? "" : orderByColumn };
            spParameters[2] = new SqlParameter() { ParameterName = "SortOrder", Value = string.IsNullOrEmpty(sortOrder) ? "" : sortOrder };
            spParameters[3] = new SqlParameter() { ParameterName = "PageIndex", Value = pageIndex };
            spParameters[4] = new SqlParameter() { ParameterName = "RowCount", Value = rowCount };
            var storeProcSearchResult = DbContextManager.SqlQueryAsync<StoreProcSearchViewModel>("EXEC dbo.spUserStoryLike @UserId,@OrderByColumn,@SortOrder,@PageIndex,@RowCount", spParameters).Result;
            return storeProcSearchResult.SingleOrDefault()?.Result;
        }
        public string GetStoryLikes(int storyId, string orderByColumn, string sortOrder, int pageIndex, int rowCount, int userId = 0)
        {
            var spParameters = new object[6];
            spParameters[0] = new SqlParameter() { ParameterName = "StoryId", Value = storyId };
            spParameters[1] = new SqlParameter() { ParameterName = "OrderByColumn", Value = string.IsNullOrEmpty(orderByColumn) ? "" : orderByColumn };
            spParameters[2] = new SqlParameter() { ParameterName = "SortOrder", Value = string.IsNullOrEmpty(sortOrder) ? "" : sortOrder };
            spParameters[3] = new SqlParameter() { ParameterName = "PageIndex", Value = pageIndex };
            spParameters[4] = new SqlParameter() { ParameterName = "RowCount", Value = rowCount };
            spParameters[5] = new SqlParameter() { ParameterName = "UserId", Value = userId };
            var storeProcSearchResult = DbContextManager.SqlQueryAsync<StoreProcSearchViewModel>("EXEC dbo.spStoryLikeViews @StoryId,@OrderByColumn,@SortOrder,@PageIndex,@RowCount,@UserId", spParameters).Result;
            return storeProcSearchResult.SingleOrDefault()?.Result;
        }

        public string EmailBody(string templateName, vStories vStories)
        {
            string body = string.Empty;
            Dictionary<string, object> keyProperties = null;
            keyProperties = new Dictionary<string, object>
                     {
                         { "##storytitle##",vStories?.Title },
                         { "##authorname##",!string.IsNullOrEmpty(vStories?.UserName)? vStories?.UserName:"" },
                         { "##date##",vStories.CreatedByDate.HasValue ? vStories?.CreatedByDate.ToString() :"" },
                      };
            body = ApplicationUtility.GetEmailTemplate(templateName, keyProperties);
            return body;
        }
    }
    public interface IStoryDomain
    {
        string Get(int userId, string searchQuery, string orderByColumn, string sortOrder, int pageIndex, int rowCount, int storyId);
        Story Get(int id);

        HashSet<string> AddValidation(Story story);
        HashSet<string> UpdateValidation(Story story);

        Story Add(Story story);
        Story Update(Story story);
        HashSet<string> DeleteValidation(int id);
        void Delete(int id);

        StoryViewModel ManageStory(StoryViewModel storyViewModel);
        StoryViewModel UpdateStory(StoryViewModel storyViewModel);
        string GetLikeUserStory(int userId, string orderByColumn, string sortOrder, int pageIndex, int rowCount);
        string GetStoryLikes(int storyId, string orderByColumn, string sortOrder, int pageIndex, int rowCount, int userId = 0);
    }
}
