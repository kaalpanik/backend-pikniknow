﻿using Microsoft.AspNetCore.SignalR;
using Rx.Core.Data;
using Rx.Core.Security;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using TravelApp.Infrastructure.Context;
using TravelApp.Infrastructure.Uow;
using TravelApp.Infrastructure.Utilities;
using TravelApp.Model.DbModel;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Domain
{
    public class StoryCommentDomain : IStoryCommentDomain
    {
        private IHubContext<WebSocketEventHub> HubContext { get; set; }
        private IApplicationUtility ApplicationUtility { get; set; }
        private HashSet<string> ValidationMessages { get; set; }

        private ITravelAppUow TravelAppUow { get; set; }
        public IFileOperations FileOperations { get; set; }
        IDbContextManager<MainSqlDbContext> DbContextManager { get; set; }
        public ICommunication Communication { get; set; }
        public StoryCommentDomain(ICommunication communication, IHubContext<WebSocketEventHub> hubContext, IDbContextManager<MainSqlDbContext> dbContextManager, IFileOperations fileOperations, ITravelAppUow travelAppUow, IApplicationUtility applicationUtility)
        {
            TravelAppUow = travelAppUow;
            ApplicationUtility = applicationUtility;
            ValidationMessages = new HashSet<string>();
            FileOperations = fileOperations;
            DbContextManager = dbContextManager;
            HubContext = hubContext;
            Communication = communication;
        }

        public string Get(string searchQuery, string orderByColumn, string sortOrder, int pageIndex, int rowCount, int storyId)
        {
            var spParameters = new object[6];
            spParameters[0] = new SqlParameter() { ParameterName = "SearchQuery", Value = string.IsNullOrEmpty(searchQuery) ? "" : searchQuery };
            spParameters[1] = new SqlParameter() { ParameterName = "OrderByColumn", Value = string.IsNullOrEmpty(orderByColumn) ? "" : orderByColumn };
            spParameters[2] = new SqlParameter() { ParameterName = "SortOrder", Value = string.IsNullOrEmpty(sortOrder) ? "" : sortOrder };
            spParameters[3] = new SqlParameter() { ParameterName = "PageIndex", Value = pageIndex };
            spParameters[4] = new SqlParameter() { ParameterName = "RowCount", Value = rowCount };
            spParameters[5] = new SqlParameter() { ParameterName = "StoryId", Value = storyId };
            var storeProcSearchResult = DbContextManager.SqlQueryAsync<StoreProcSearchViewModel>("EXEC dbo.spStoryComments @SearchQuery,@OrderByColumn,@SortOrder,@PageIndex,@RowCount,@StoryId", spParameters).Result;
            return storeProcSearchResult.SingleOrDefault()?.Result;
        }

        public StoryComment Get(int id)
        {
            var storyComment = TravelAppUow.Repository<StoryComment>()
                                        .FindBy(x => x.StoryCommentId == id).FirstOrDefault();
            return storyComment;
        }

        public HashSet<string> AddValidation(StoryComment storyComment)
        {
            return this.ValidationMessages;
        }

        public HashSet<string> UpdateValidation(StoryComment storyComment)
        {
            return this.ValidationMessages;
        }

        public StoryComment Add(StoryComment storyComment)
        {
            //storyComment.CreatedByDate = DateTime.Now;
            TravelAppUow.RegisterNew<StoryComment>(storyComment);
            TravelAppUow.Commit();
            vUser vUser = TravelAppUow.Repository<vUser>().FindBy(x => x.UserId == storyComment.CreatedById).FirstOrDefault();
            vStories vStories = TravelAppUow.Repository<vStories>().FindBy(x => x.StoryId == storyComment.StoryId).FirstOrDefault();
            if (vUser != null && vStories != null)
            {
                string body = vUser.UserName + " Comment on your story " + vStories.Title;
                List<vApplicationUserToken> vApplicationUserToken = TravelAppUow.Repository<vApplicationUserToken>().FindBy(x => x.UserId == vStories.CreatedById).ToList();
                if (vApplicationUserToken.Count() > 0)
                {
                    Communication.SendNotification(vStories.CreatedById,NotificationType.StoryComment, "Story Comment", body, vApplicationUserToken);
                }
               // var istdate = TimeZoneInfo.ConvertTimeFromUtc(storyComment.CreatedByDate, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                var date = storyComment.CreatedByDate.ToString("yyyy-MM-dd HH:mm:ss");
                var result = new
                {
                    storyComment.Comment,
                    storyComment.StoryId,
                    storyComment.CreatedById,
                    storyComment.ParentStoryCommentId,
                    isReplay = storyComment.ParentStoryCommentId > 0 ? true : false,
                    storyComment.StoryCommentId,
                    CreatedByDate = date,
                    vUser.UserName,
                    vUser.ProfilePicturePath
                };
                var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                HubContext.Clients.All.SendAsync("storyComment", result);
            }
            return storyComment;
        }

        public StoryComment Update(StoryComment storyComment)
        {
            TravelAppUow.RegisterDirty<StoryComment>(storyComment);
            TravelAppUow.Commit();
            return storyComment;
        }

        public HashSet<string> DeleteValidation(int id)
        {
            return this.ValidationMessages;
        }

        public void Delete(int id)
        {
            var storyComment = TravelAppUow.Repository<StoryComment>()
                                        .FindBy(x => x.StoryCommentId == id).FirstOrDefault();
            if (storyComment != null)
            {
                TravelAppUow.RegisterDeleted<StoryComment>(storyComment);
                TravelAppUow.Commit();
            }
        }

    }
    public interface IStoryCommentDomain
    {
        string Get(string searchQuery, string orderByColumn, string sortOrder, int pageIndex, int rowCount, int storyId);
        StoryComment Get(int id);

        HashSet<string> AddValidation(StoryComment storyComment);
        HashSet<string> UpdateValidation(StoryComment storyComment);

        StoryComment Add(StoryComment storyComment);
        StoryComment Update(StoryComment storyComment);
        HashSet<string> DeleteValidation(int id);
        void Delete(int id);


    }
}
