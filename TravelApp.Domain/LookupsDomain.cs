﻿using Rx.Core.Data;
using Rx.Core.Security;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TravelApp.Infrastructure.Context;
using TravelApp.Model.ViewModel;

namespace TravelApp.Domain
{
   public class LookupsDomain: ILookupsDomain
    {
        IDbContextManager<MainSqlDbContext> DbContextManager { get; set; }
        public LookupsDomain(IDbContextManager<MainSqlDbContext> dbContextManager)
        {
            DbContextManager = dbContextManager;
        }
        public string Get(string searchQuery)
        {
            var spParameters = new object[1];
            spParameters[0] = new SqlParameter() { ParameterName = "SearchQuery", Value = string.IsNullOrEmpty(searchQuery) ? "" : searchQuery };
            var storeProcSearchResult = DbContextManager.SqlQueryAsync<StoreProcSearchViewModel>("EXEC dbo.spLookups @SearchQuery", spParameters).Result;
            return storeProcSearchResult.SingleOrDefault()?.Result;
        }
        public string GetDashboard()
        {
            var spParameters = new object[1];
            spParameters[0] = new SqlParameter() { ParameterName = "UserId", Value = UserClaim.UserId };
            var storeProcSearchResult = DbContextManager.SqlQueryAsync<StoreProcSearchViewModel>("EXEC dbo.spDashboard @UserId", spParameters).Result;
            return storeProcSearchResult.SingleOrDefault()?.Result;
        }
    }
    public interface ILookupsDomain
    {
        string Get(string searchQuery);
        string GetDashboard();
    }
}
