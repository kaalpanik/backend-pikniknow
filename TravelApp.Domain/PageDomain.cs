﻿using Rx.Core.Data;
using Rx.Core.Security;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using TravelApp.Infrastructure.Context;
using TravelApp.Infrastructure.Uow;
using TravelApp.Infrastructure.Utilities;
using TravelApp.Model.DbModel;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;
namespace TravelApp.Domain
{
    public class PageDomain: IPageDomain
    {
        private ITravelAppUow TravelAppUow { get; set; }
        public IFileOperations FileOperations { get; set; }
        IDbContextManager<MainSqlDbContext> DbContextManager { get; set; }
        private IApplicationUtility ApplicationUtility { get; set; }
        private HashSet<string> ValidationMessages { get; set; }
        public PageDomain(IDbContextManager<MainSqlDbContext> dbContextManager, IFileOperations fileOperations, ITravelAppUow travelAppUow, IApplicationUtility applicationUtility)
        {
            TravelAppUow = travelAppUow;
            ApplicationUtility = applicationUtility;
            ValidationMessages = new HashSet<string>();
            FileOperations = fileOperations;
            DbContextManager = dbContextManager;
        }

        public IEnumerable<vPages> Get()
        {
            var page = TravelAppUow.Repository<vPages>().All().AsQueryable();
            return page;
        }

        public Page Get(int id)
        {
            var page = TravelAppUow.Repository<Page>()
                                        .FindBy(x => x.PageId == id).FirstOrDefault();
            return page;
        }

        public vPages GetPageByLocationId(int id)
        {
            var page = TravelAppUow.Repository<vPages>()
                                        .FindBy(x => x.PageLocationId == id).FirstOrDefault();
            return page;
        }

        public HashSet<string> AddValidation(Page page)
        {
            var isPageExist = TravelAppUow.Repository<Page>()
                                       .FindBy(x => x.PageLocationId == page.PageLocationId).Any();
            if (isPageExist)
            {
                ValidationMessages.Add("Page already added");
            }
            return ValidationMessages;
        }

        public HashSet<string> UpdateValidation(Page page)
        {
            var isPageExist = TravelAppUow.Repository<Page>()
                                       .FindBy(x => x.PageId !=page.PageId && x.PageLocationId == page.PageLocationId).Any();
            if (isPageExist)
            {
                ValidationMessages.Add("Page already added");
            }
            return ValidationMessages;
        }

        public Page Add(Page page)
        {
            TravelAppUow.RegisterNew<Page>(page);
            TravelAppUow.Commit();
            return page;
        }

        public Page Update(Page page)
        {
            TravelAppUow.RegisterDirty<Page>(page);
            TravelAppUow.Commit();
            return page;
        }

        public HashSet<string> DeleteValidation(int id)
        {
            return ValidationMessages;
        }

        public void Delete(int id)
        {
            var page = TravelAppUow.Repository<Page>()
                                        .FindBy(x => x.PageId == id).FirstOrDefault();
            if (page != null)
            {
                TravelAppUow.RegisterDeleted<Page>(page);
                TravelAppUow.Commit();
            }
        }
    }
    public interface IPageDomain
    {
        IEnumerable<vPages> Get();
        Page Get(int id);

        vPages GetPageByLocationId(int id);

        HashSet<string> AddValidation(Page page);
        HashSet<string> UpdateValidation(Page page);

        Page Add(Page page);
        Page Update(Page page);
        HashSet<string> DeleteValidation(int id);
        void Delete(int id);
    }
}
