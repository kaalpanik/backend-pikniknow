﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Rx.Core.Security;
using System;
using System.Collections.Generic;
using System.Text;
using TravelApp.Infrastructure.Uow;
using TravelApp.Model.DbModel;
using TravelApp.Model.ViewModel;

namespace TravelApp.Infrastructure.ExceptionLogs
{
    public class LogException: ILogException
    {
        private IApplicationExceptionUow Uow { get; set; }
        public LogException(IApplicationExceptionUow exceptionUow)
        {
            Uow = exceptionUow;
        }

        private string DatabaseLog(Exception exception, string uri)
        {

            ApplicationExceptionLog applicationExceptionLog = new ApplicationExceptionLog
            {
                Url = uri,
                Message = exception.Message.ToString(),
                ExceptionType = exception.GetType().ToString(),
                ExceptionSource = exception.Source ?? string.Empty,
                StackTrace = exception.StackTrace,
                InnerException = (exception.InnerException != null) ? Convert.ToString(exception.InnerException) : string.Empty,
                ExceptionDate = DateTime.UtcNow,
                UserId = 5
            };

            Uow.RegisterNew<ApplicationExceptionLog>(applicationExceptionLog);
            Uow.Commit();
            string error = string.Format("User : {0}<br/> Date & Time : {1}<br/> Error Log Id : {2}",
                    5,
                    DateTime.UtcNow.ToString(),
                    applicationExceptionLog.ApplicationExceptionLogId.ToString());

            APIResponse result = new APIResponse()
            {
                Response = applicationExceptionLog,
                Message = "An error occurred, please contact the site administrator.",
                Status = false
            };
            string jsonResult = JsonConvert.SerializeObject(result, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });

            return jsonResult;

        }

        public string Log(Exception exception, string uri)
        {
            return DatabaseLog(exception, uri);
        }
    }
    public interface ILogException
    {
        string Log(Exception exception, string uri);
    }
}
