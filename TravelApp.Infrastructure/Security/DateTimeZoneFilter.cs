﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using NodaTime;
using Rx.Core.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace TravelApp.Infrastructure.Security
{
    public class DateTimeZoneFilter : ActionFilterAttribute
    {
        private IPrincipal Principal { get; set; }
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            this.Principal = (IPrincipal)context.HttpContext.RequestServices.GetService(typeof(IPrincipal));
            var objectResult = ((ObjectResult)context.Result).Value;
            var typeOfValue = objectResult.GetType();
            if (typeOfValue.IsConstructedGenericType)
            {
                var objectCollection = (IEnumerable)objectResult;
                var enumerator = objectCollection.GetEnumerator();
                var parameter = typeOfValue.GenericTypeArguments[0];
                var properties = GetZoneProperties(parameter);
                if (properties != null && properties.Count() > 0)
                {
                    while (enumerator.MoveNext())
                    {
                        var result = enumerator.Current;
                        SetTimeZoneDateTime(properties, result);
                    };
                }
            }
            else
            {
                if (typeOfValue.IsClass)
                {
                    var properties = GetZoneProperties(typeOfValue);
                    if (properties != null && properties.Count() > 0)
                        SetTimeZoneDateTime(properties, objectResult);
                }
            }
        }

        private void SetTimeZoneDateTime(IEnumerable<PropertyInfo> properties, object result)
        {
            foreach (var property in properties)
            {
                var dateValue = property.GetValue(result);
                var transformDateTime = this.GetZoneDateTime(dateValue);
                if (transformDateTime != DateTime.MinValue)
                    property.SetValue(result, transformDateTime);
            }
        }

        private IEnumerable<PropertyInfo> GetZoneProperties(Type instance)
        {
            var customAttribute = instance.GetCustomAttributes(typeof(TimeZoneDateConverterBaseAttribute), false).SingleOrDefault();
            if (customAttribute != null)
            {
                return ((PropertyInfo[])instance.GetProperties()).Where(p => p.GetCustomAttributes(typeof(TimeZoneDateConverterAttribute), false).Count() > 0);
            }
            return null;
        }

        private DateTime GetZoneDateTime(object dateValue)
        {
            if (dateValue is DateTime)
            {
                DateTime dateObject = Convert.ToDateTime(dateValue);
                if (dateObject > DateTime.MinValue)
                {

                    var userTimeZone = GetUserTimeZone(this.Principal);
                    if (!string.IsNullOrEmpty(userTimeZone))
                    {
                        var zoneProvider = DateTimeZoneProviders.Tzdb[userTimeZone];
                        var instant = Instant.FromDateTimeUtc(DateTime.SpecifyKind(dateObject, DateTimeKind.Utc));
                        var result = instant.InZone(zoneProvider).ToDateTimeUnspecified();
                        return result;
                    }
                }
            }
            return DateTime.MinValue;
        }

        private string GetUserTimeZone(IPrincipal principal)
        {
            //var x = ClaimsPrincipal.
            var claimObject = ((System.Security.Claims.ClaimsPrincipal)(principal)).Claims.Where(t => t.Type == ClaimTypes.Locality).SingleOrDefault();
            return (claimObject != null) ? claimObject.Value : string.Empty;
        }
    }
}
