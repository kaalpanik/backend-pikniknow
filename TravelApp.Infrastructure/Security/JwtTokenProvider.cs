﻿using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Rx.Core.Security.Jwt;
using Rx.Core.Settings;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using TravelApp.Infrastructure.Uow;
using TravelApp.Model.DbModel;

namespace TravelApp.Infrastructure.Security
{
    public class JwtTokenProvider : TokenProvider, IJwtTokenProvider
    {
        public JwtTokenProvider(IHttpContextAccessor context, ServerSetting serverSetting) : base(context, serverSetting)
        {

        }

        public void LogOut()
        {

            if (HttpContextAccessor.HttpContext.Request.Headers.ContainsKey(HeaderNames.Authorization))
            {
                ITravelAppUow loginUow = (ITravelAppUow)HttpContextAccessor.HttpContext.RequestServices.GetService(typeof(ITravelAppUow));
                string jwtToken = HttpContextAccessor.HttpContext.Request.Headers[HeaderNames.Authorization].ToString();
                ApplicationUserToken applicationToken = loginUow.Repository<ApplicationUserToken>().SingleOrDefault(x => x.JwtToken == jwtToken);
                if (applicationToken != null)
                {
                    try
                    {
                        bool response = base.ValidateToken(applicationToken.SecurityKey, applicationToken.JwtToken);
                        if (response)
                        {
                            loginUow.RegisterDeleted<ApplicationUserToken>(applicationToken);
                            loginUow.Commit();
                        }
                    }
                    catch (Exception e) when (e is SecurityTokenExpiredException)
                    {
                        loginUow.RegisterDeleted<ApplicationUserToken>(applicationToken);
                        loginUow.Commit();
                        throw new Exception("Token is expired");
                    }
                }
                else
                {
                    throw new Exception("Token is not available in database");
                }
            }
        }

        public ApplicationUserToken WriteToken(User user)
        {
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.NameIdentifier, (user.UserId).ToString()));
            claims.Add(new Claim(ClaimTypes.Name, user?.MobileNumber));
            claims.Add(new Claim(ClaimTypes.Role, (user.RoleId).ToString()));
            claims.Add(new Claim(ClaimTypes.Locality, (1).ToString()));
            claims.Add(new Claim(ClaimTypes.System, "AWS"));
            claims.Add(new Claim(ClaimTypes.GivenName, "Travel"));
            claims.Add(new Claim(ClaimTypes.GroupSid, 1.ToString()));
            claims.Add(new Claim(ClaimTypes.UserData, 1.ToString()));
            claims.Add(new Claim(ClaimTypes.Expiration, 5.ToString()));
            DateTime expires = DateTime.UtcNow.AddHours(365);
            KeyValuePair<byte[], string> token = base.WriteToken(claims, "WestStarEvents", "Web", expires);
            bool response = base.ValidateToken(token.Key, token.Value);
            ApplicationUserToken applicationUserToken = new ApplicationUserToken
            {
                AccessedPlatform = "web",
                CreatedDateTime = DateTime.UtcNow,
                IsActive = true,
                JwtToken = token.Value,
                SecurityKey = token.Key,
                ExpiresAt = expires,
                UserId = user.UserId,
                TokenIssuer = "Travel",
            };

            return applicationUserToken;
        }

        public bool Validate(HttpContext httpContext)
        {
            if (httpContext.Request.Headers.ContainsKey(HeaderNames.Authorization))
            {
                ITravelAppUow loginUow = (ITravelAppUow)httpContext.RequestServices.GetService(typeof(ITravelAppUow));
                string jwtToken = httpContext.Request.Headers[HeaderNames.Authorization].ToString();
                ApplicationUserToken applicationToken = loginUow.Repository<ApplicationUserToken>().SingleOrDefault(x => x.JwtToken == jwtToken);
                if (applicationToken != null)
                {
                    try
                    {
                        bool response = base.ValidateToken(applicationToken.SecurityKey, applicationToken.JwtToken);
                        if (response)
                        {
                            if (httpContext.Request.Headers.ContainsKey("x-session"))
                            {
                                string session = httpContext.Request.Headers["x-session"].ToString();
                                DateTime dateConvert = Convert.ToDateTime(session);
                                return (DateTime.UtcNow < dateConvert);
                            }
                            return response;
                        }
                        return response;
                    }
                    catch (Exception e) when (e is SecurityTokenExpiredException)
                    {
                        loginUow.RegisterDeleted<ApplicationUserToken>(applicationToken);
                        loginUow.Commit();
                        throw new Exception("Token is expired");
                    }

                }
                else
                {
                    throw new Exception("Token is not available in database");
                }
            }
            return false;
        }
    }
    public interface IJwtTokenProvider : ITokenValidator
    {
        ApplicationUserToken WriteToken(User user);
        void LogOut();
    }
}
