﻿using Microsoft.AspNetCore.Http;
using Rx.Core.Data;
using System;
using System.Collections.Generic;
using System.Text;
using TravelApp.Infrastructure.Context;

namespace TravelApp.Infrastructure.Security
{
    public class UserAuthorization : IUserAuthorization
    {
        private IDbContextManager<MainSqlDbContext> DbContextManager { get; set; }
        private IHttpContextAccessor ContextAccessor { get; set; }
        public UserAuthorization(IHttpContextAccessor contextAccessor)
        {
            ContextAccessor = contextAccessor;
        }
        public Dictionary<string, Dictionary<string, object>> GetAccessModules(int applicationModuleId, int roleId)
        {
            DbContextManager = (IDbContextManager<MainSqlDbContext>)ContextAccessor.HttpContext.RequestServices.GetService(typeof(IDbContextManager<MainSqlDbContext>));
            //var dicObject = new Dictionary<string, object>();
            var dicObject = new Dictionary<string, Dictionary<string, object>>();
            
            return dicObject;
        }

    }
    public interface IUserAuthorization
    {
        Dictionary<string, Dictionary<string, object>> GetAccessModules(int moduleId, int roleId);
    }
}
