﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Rx.Core.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace TravelApp.Infrastructure.Security
{
    public class SessionTimeOut : ActionFilterAttribute
    {
        ServerSetting ServerSetting;
        public SessionTimeOut(ServerSetting serverSetting)
        {
            ServerSetting = serverSetting;
        }
        private double SessionTimeOutValue { get; set; }
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            SetSession(context.HttpContext);
            base.OnActionExecuted(context);
        }

        private void SetSession(HttpContext context)
        {
            SessionTimeOutValue = ServerSetting.Get<double>("session.expireMinutes");
            DateTime epochStart = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, DateTime.UtcNow.Hour, DateTime.UtcNow.Minute, 0, 0, DateTimeKind.Local).AddMinutes(Convert.ToDouble(SessionTimeOutValue));
            context.Response.Headers.Add("x-session", new[] { epochStart.ToString() });
        }
    }
}
