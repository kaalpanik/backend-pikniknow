﻿using Rx.Core.Data;
using System;
using System.Collections.Generic;
using System.Text;
using TravelApp.Infrastructure.Context;

namespace TravelApp.Infrastructure.Uow
{
    public class ApplicationExceptionUow : CoreUnitOfWork, IApplicationExceptionUow
    {
        public ApplicationExceptionUow(IExceptionLogContext context, IRepositoryProvider repositoryProvider)
        {
            base.SetContextRepository(context, repositoryProvider);
        }
    }
    public interface IApplicationExceptionUow : ICoreUnitOfWork
    { }
}
