﻿using Rx.Core.Data;
using System;
using System.Collections.Generic;
using System.Text;
using TravelApp.Infrastructure.Context;

namespace TravelApp.Infrastructure.Uow
{
    public class TravelAppUow : CoreUnitOfWork, ITravelAppUow
    {
        public TravelAppUow(ITravelAppContext context, IRepositoryProvider repositoryProvider)
        {
            base.SetContextRepository(context, repositoryProvider);
        }
    }
    public interface ITravelAppUow : ICoreUnitOfWork
    { }
}
