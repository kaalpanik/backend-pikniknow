﻿using Rx.Core.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TravelApp.Infrastructure.Utilities
{
    public class ApplicationUtility: IApplicationUtility
    {
        public ServerSetting ServerSetting { get; set; }
        public ApplicationUtility(ServerSetting serverSetting)
        {
            ServerSetting = serverSetting;
        }
        public string GenerateRandomOTP()
        {
            string[] saAllowedCharacters = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "0" };
            string sOTP = String.Empty;
            string sTempChars = String.Empty;
            Random rand = new Random();
            for (int i = 0; i < 5; i++)
            {
                int p = rand.Next(0, saAllowedCharacters.Length);
                sTempChars = saAllowedCharacters[rand.Next(0, saAllowedCharacters.Length)];
                sOTP += sTempChars;
            }
            return sOTP;
        }
        public string GetEmailTemplate(string templateName, Dictionary<string, object> properties)
        {
            string body = string.Empty;
            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/EmailTemplates/" + templateName);
            StreamReader reader = new StreamReader(path);
            body = reader.ReadToEnd();
            foreach (var item in properties)
            {
                body = body.Replace(item.Key.ToString(), item.Value.ToString());
            }
            body = body.Replace("##siteName##", ServerSetting.Get<string>("EmailTemplates.Settings.SiteName"));
            body = body.Replace("##domainEmail##", ServerSetting.Get<string>("EmailTemplates.Settings.EmailDomain"));
            return body;
        }
    }
    public interface IApplicationUtility
    {
        string GenerateRandomOTP();
        string GetEmailTemplate(string templateName, Dictionary<string, object> properties);
    }
}
