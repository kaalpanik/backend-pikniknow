﻿using Newtonsoft.Json;
using Rx.Core.Data;
using Rx.Core.Settings;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using TravelApp.Infrastructure.Context;
using TravelApp.Infrastructure.Uow;
using TravelApp.Model.DbModel;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Infrastructure.Utilities
{
    public class Communication : ICommunication
    {
        private IDbContextManager<MainSqlDbContext> DbContextManager { get; set; }
        private ITravelAppUow TravelAppUow { get; set; }
        public ServerSetting ServerSetting { get; set; }
        public string SMSAPIKey = string.Empty;
        public Communication(IDbContextManager<MainSqlDbContext> dbContextManager,ITravelAppUow travelAppUow, ServerSetting serverSetting)
        {
            ServerSetting = serverSetting;
            SMSAPIKey = ServerSetting.Get<string>("SMS.apiKey");
            TravelAppUow = travelAppUow;
            DbContextManager = dbContextManager;
        }

        public async Task<SMSViewModel> Send(string mobileNumber)
        {
            SMSViewModel sMSViewModel = new SMSViewModel();
            try
            {
                string apiUrl = ServerSetting.Get<string>("SMS.endPointSend");
                apiUrl = String.Format(apiUrl, SMSAPIKey, mobileNumber);
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(apiUrl);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                sMSViewModel = JsonConvert.DeserializeObject<SMSViewModel>(responseBody);
                sMSViewModel.Flag = true;
                return sMSViewModel;
            }
            catch (Exception e)
            {
                sMSViewModel.Flag = false;
                return sMSViewModel; 
            }
        }

        public async Task<SMSViewModel> Verify(string sessionId, string otpCode)
        {
            SMSViewModel sMSViewModel = new SMSViewModel();
            try
            {
                string apiUrl = ServerSetting.Get<string>("SMS.endPointVerify");
                apiUrl = String.Format(apiUrl, SMSAPIKey, sessionId, otpCode);
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(apiUrl);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                sMSViewModel = JsonConvert.DeserializeObject<SMSViewModel>(responseBody);
                sMSViewModel.Flag = true;
                return sMSViewModel;
            }
            catch (Exception e)
            {
                sMSViewModel.Flag = false;
                return sMSViewModel; ;
            }
        }

        public async Task SendNotification(int userId,NotificationType notificationType, string notificationTitle, string body, List<vApplicationUserToken> tokens)
        {
            String[] variable_name = new String[tokens.Count()];
            int i = 0;
            foreach (var item in tokens)
            {
                variable_name[i] = item.FCMToken;
                i++;
            }

            var message = new
            {
                registration_ids = variable_name.ToArray(),
                notification = new
                {
                    title = notificationTitle,
                    body = body,
                    type = notificationType
                },
                priority = 10
            };
            string jsonMessage = JsonConvert.SerializeObject(message);
            var result = "-1";
            var webAddr = ServerSetting.Get<string>("fcm.url");
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add("Authorization:key=" + ServerSetting.Get<string>("fcm.serverKey"));
            httpWebRequest.Method = "POST";

            using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(jsonMessage);
                streamWriter.Flush();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var spParameters = new SqlParameter[4];
                spParameters[0] = new SqlParameter() { ParameterName = "UserId", Value = userId };
                spParameters[1] = new SqlParameter() { ParameterName = "Title", Value = message.notification.title };
                spParameters[2] = new SqlParameter() { ParameterName = "Content", Value = message.notification.body };
                spParameters[3] = new SqlParameter() { ParameterName = "NotificationType", Value = message.notification.type };
                var storeProcSearchResult = DbContextManager.SqlQueryAsync<StoreProcSearchViewModel>("exec spInsertUserNotification @UserId,@Title,@Content,@NotificationType", spParameters).Result;
                var data = storeProcSearchResult.SingleOrDefault()?.Result;
                result = await streamReader.ReadToEndAsync();
            }
        }

        public void SendSMTPMail(string mailTo, string mailSubject, string mailBody, string mailCC = "", string mailBCC = "", string filePath = "")
        {
            //mailTo = "lohar.devendra1994@gmail.com";
            var smtpConfiguration = TravelAppUow.Repository<SMTPConfiguration>().FirstOrDefault(x => x.IsActive == true);
            MailMessage message = new MailMessage();
            MailAddress mailAddress = new MailAddress(smtpConfiguration.FromEmail, "Pikniknow");
            message.From = mailAddress;
            message.Subject = mailSubject;
            message.Body = mailBody;
            message.IsBodyHtml = true;

            if (!(filePath == string.Empty || filePath == null))
            {
                string[] strAttachment = filePath.Split(';');

                foreach (string strThisAttachment in strAttachment)
                {
                    byte[] data = System.Convert.FromBase64String(strThisAttachment);
                    Stream stream = new MemoryStream(data);
                    if (!strThisAttachment.Equals(""))
                    {
                        message.Attachments.Add(new Attachment(stream, "test"));
                    }
                }

            }

            if (!(mailTo == string.Empty || mailTo == null))
            {
                string[] strTo = mailTo.Split(';');

                foreach (string strThisTo in strTo)
                {
                    if (!strThisTo.Equals(""))
                    {
                        message.To.Add(strThisTo.Trim());
                    }
                }
            }

            if (!(mailCC == string.Empty || mailCC == null))
            {
                string[] strCC = mailCC.Split(';');

                foreach (string strThisCC in strCC)
                {
                    if (!strThisCC.Equals(""))
                    {
                        message.CC.Add(strThisCC.Trim());
                    }
                }
            }

            if (!(mailBCC == string.Empty || mailBCC == null))
            {
                string[] strBCC = mailBCC.Split(';');

                foreach (string strThisBCC in strBCC)
                {
                    if (!strThisBCC.Equals(""))
                    {
                        message.Bcc.Add(strThisBCC.Trim());
                    }
                }
            }

            var fromAddress = new MailAddress(smtpConfiguration.FromEmail, "Pikniknow");

            SmtpClient mySmtpClient = new SmtpClient
            {
                Host = smtpConfiguration.Host,
                Port = Convert.ToInt32(smtpConfiguration.Port),
                EnableSsl = Convert.ToBoolean(smtpConfiguration.EnableSSL),
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = Convert.ToBoolean(smtpConfiguration.DefaultCredentials),
                Credentials = new NetworkCredential(smtpConfiguration.UserName, smtpConfiguration.Password)
            };
            try
            {
                mySmtpClient.Send(message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            message.Dispose();
            mySmtpClient = null;
        }
    }
    public interface ICommunication
    {
        Task<SMSViewModel> Send(string mobileNumber);
        Task<SMSViewModel> Verify(string sessionId, string otpCode);

        Task SendNotification(int userId, NotificationType notificationType,  string notificationTitle, string body, List<vApplicationUserToken> tokens);

        void SendSMTPMail(string mailTo, string mailSubject, string mailBody, string mailCC = "", string mailBCC = "", string filePath = "");

    }

}
