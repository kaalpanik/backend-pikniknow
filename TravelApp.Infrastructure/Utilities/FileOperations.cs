﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using Microsoft.AspNetCore.Http;
using Rx.Core.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TravelApp.Model.ViewModel;

namespace TravelApp.Infrastructure.Utilities
{
    public class FileOperations : IFileOperations
    {
        private ServerSetting ServerSetting { get; set; }
        public FileOperations(ServerSetting serverSetting)
        {
            ServerSetting = serverSetting;
        }
        // Save file to blob storage
        public string SaveFile(FileDetailModel fileDetail, string azureStorageContainer, string directoryName = "UserProfiles")
        {
            //var fileTransferUtility = new TransferUtility(s3Client);
            //string fileUrl = string.Empty;
            //if (!string.IsNullOrEmpty(fileDetail.FileName))
            //{
            //    byte[] bytes = Convert.FromBase64String(fileDetail.FileData); ;
            //    using (var memoryStream = new MemoryStream())
            //    {
            //        Stream stream = stream1.CopyTo(memoryStream);
            //        bytes = memoryStream.ToArray();
            //    }

            //    string base64 = Convert.ToBase64String(bytes);
            //    var data = new MemoryStream(Encoding.UTF8.GetBytes(base64));
            //}

            return "";
        }

        public async Task UploadFileToS3(IFormFile file, string fileName)
        {
            try
            {
                using (var client = new AmazonS3Client(ServerSetting.Get<string>("s3.accessKey"), ServerSetting.Get<string>("s3.secretAccessKey"), RegionEndpoint.APSouth1))
                {
                    using (var newMemoryStream = new MemoryStream())
                    {
                        file.CopyTo(newMemoryStream);

                        var uploadRequest = new TransferUtilityUploadRequest
                        {
                            InputStream = newMemoryStream,
                            Key = fileName,
                            BucketName = ServerSetting.Get<string>("s3.bukerName"),
                            CannedACL = S3CannedACL.PublicRead
                        };

                        var fileTransferUtility = new TransferUtility(client);
                        await fileTransferUtility.UploadAsync(uploadRequest);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
          
        }
        
        public bool DeleteFile(string blobName, string azureStorageContainer)
        {
            bool fileDelete = false;
            if (!string.IsNullOrEmpty(blobName))
            {
                fileDelete = true;
            }
            return fileDelete;
        }

    }
    public interface IFileOperations
    {
        string SaveFile(FileDetailModel fileDetail, string azureStorageContainer, string directoryName = "UserProfiles");
        bool DeleteFile(string blobName, string azureStorageContainer);
        Task UploadFileToS3(IFormFile file,string fileName);
    }
}
