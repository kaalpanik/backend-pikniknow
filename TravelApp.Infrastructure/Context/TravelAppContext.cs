using Microsoft.EntityFrameworkCore;
using Rx.Core.Data;
using TravelApp.Model.DbModel;

namespace TravelApp.Infrastructure.Context
{
    public class TravelAppContext : DbContext, ITravelAppContext
    {

        public TravelAppContext(MainSqlDbContext mainSqlDbContext)
        {
            MainSqlDbContext = mainSqlDbContext;
            this.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        public DbSet<User> User { get; set; }
        public DbSet<StoryMedia> StoryMedia { get; set; }
        public DbSet<StoryTag> StoryTag { get; set; }
        public DbSet<Tag> Tag { get; set; }
        public DbSet<UserFollower> UserFollower { get; set; }
        public DbSet<StoryComment> StoryComment { get; set; }
        public DbSet<Story> Story { get; set; }
        public DbSet<SMTPConfiguration> SMTPConfiguration { get; set; }
        public DbSet<RolePermission> RolePermission { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<RequestLog> RequestLog { get; set; }
        public DbSet<GlobalSetting> GlobalSetting { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<AuditRecord> AuditRecord { get; set; }
        public DbSet<ApplicationUserToken> ApplicationUserToken { get; set; }
        public DbSet<ApplicationTimeZone> ApplicationTimeZone { get; set; }
        public DbSet<ApplicationObjectType> ApplicationObjectType { get; set; }
        public DbSet<ApplicationObject> ApplicationObject { get; set; }
        public DbSet<ApplicationModule> ApplicationModule { get; set; }
        public DbSet<ApplicationExceptionLog> ApplicationExceptionLog { get; set; }

        public DbSet<UserStory> UserStory { get; set; }
        public DbSet<vApplicationUserToken> vApplicationUserToken { get; set; }
        public DbSet<vUser> vUser { get; set; }


        
        public DbSet<Page> Pages { get; set; }

        public DbSet<vPages> vPages { get; set; }

        public DbSet<vStories> vStories { get; set; }

        public DbSet<Banner> Banners { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(MainSqlDbContext.Database.GetDbConnection());
            base.OnConfiguring(optionsBuilder);
        }

        private MainSqlDbContext MainSqlDbContext;
    }
    public interface ITravelAppContext : IDbContext
    { }
}
