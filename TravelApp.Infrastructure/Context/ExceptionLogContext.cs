using Microsoft.EntityFrameworkCore;
using Rx.Core.Data;
using TravelApp.Model.DbModel;

namespace TravelApp.Infrastructure.Context
{
    public class ExceptionLogContext : DbContext, IExceptionLogContext
    {
        public ExceptionLogContext(MainSqlDbContext mainSqlDbContext)
        {
            MainSqlDbContext = mainSqlDbContext;
            this.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        
        public DbSet<ApplicationExceptionLog> ApplicationExceptionLog { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(MainSqlDbContext.Database.GetDbConnection());
            base.OnConfiguring(optionsBuilder);
        }

        private MainSqlDbContext MainSqlDbContext;
    }
    public interface IExceptionLogContext : IDbContext
    { }
}
