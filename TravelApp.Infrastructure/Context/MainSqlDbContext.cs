using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Rx.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TravelApp.Model.ViewModel;

namespace TravelApp.Infrastructure.Context
{
  public class MainSqlDbContext: BaseDbContext, IDisposable
  {
    public MainSqlDbContext(ServerSetting serverSetting) : base(serverSetting) { }

    public DbSet<StoreProcSearchViewModel> StoreProcResult { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
            optionsBuilder.UseSqlServer(this.GetConnection("Main"));

            base.OnConfiguring(optionsBuilder);
    }

  }
}
