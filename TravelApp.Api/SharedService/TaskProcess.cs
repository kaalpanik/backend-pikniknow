﻿using Rx.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelApp.Api.SharedService
{
    public static class TaskProcess
    {
        public static Dictionary<string, string> Keys { get; set; }
        public static void Start(string executablePath, ServerSetting serverSetting)
        {
            Keys = new Dictionary<string, string>();
            Task.Run(() => { });
        }
    }
}
