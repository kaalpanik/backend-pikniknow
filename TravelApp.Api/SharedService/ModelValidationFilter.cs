﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using Rx.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TravelApp.Model.ViewModel;

namespace TravelApp.Api.SharedService
{
    public class ModelValidationFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            ModelStateDictionary modelState = context.ModelState;
            JObject errors = new JObject();
            if (!modelState.IsValid)
            {
                foreach (string key in modelState.Keys)
                {
                    ModelStateEntry state = modelState[key];
                    if (state.Errors.Any())
                    {
                        string camelKey = ConvertCamelCase(key);
                        if (string.IsNullOrEmpty(state.Errors.First().ErrorMessage) && state.Errors.First().Exception != null)
                        {
                            errors[((Newtonsoft.Json.JsonReaderException)state.Errors.First().Exception).Path] = state.Errors.First().Exception.Message;
                        }
                        else
                        {
                            errors[camelKey] = state.Errors.First().ErrorMessage;
                        }

                    }
                }
                this.ErrorResponse(errors, context);
            }
            else
            {
                if (context.HttpContext.Request.Method.ToUpper() == "POST" || context.HttpContext.Request.Method.ToUpper() == "PUT")
                {
                    ControllerBase controller = (ControllerBase)context.Controller;
                    if (context.ActionArguments.Count > 0)
                    {
                        foreach (KeyValuePair<string, object> actionArgument in context.ActionArguments)
                        {
                            if (actionArgument.Value is IDefaultData)
                                ((IDefaultData)actionArgument.Value).ApplyDefault();
                        }
                    }
                }
                base.OnActionExecuting(context);
            }
        }

        private string ConvertCamelCase(string key)
        {
            if (key != null && key != "")
            {
                string camelCase = "";
                string firstChar = key.Substring(0, 1);
                string postString = key.Substring(1, key.Length - 1);
                camelCase = firstChar.ToLower() + postString;
                return camelCase;
            }
            return key;
        }

        private void ErrorResponse(JObject errors, ActionExecutingContext context)
        {
            string jsonResult = JsonConvert.SerializeObject(new APIResponse()
            {
                Status = false,
                Message = errors.ToString()
            }, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });

            context.Result = new ContentResult()
            {
                ContentType = "application/json",
                Content = jsonResult
            };
            context.HttpContext.Response.StatusCode = 400;
        }
    }
}
