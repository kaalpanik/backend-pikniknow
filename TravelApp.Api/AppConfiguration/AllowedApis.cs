using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TravelApp.Api.AppConfiguration;
using TravelApp.Api.Controllers;

namespace TravelApp.Api
{
    public static class ApplicationApi
    {
        public static List<string> AuthenitcationByPass()
        {
            return new List<string> {
                    typeof(UserAuthenticationController).Name,
                    typeof(EventHub).Name,
                };
        }

        public static List<string> AuthorizationByPass()
        {
            var byPassApis = new List<string> {
                    typeof(UserAuthenticationController).Name,
                    typeof(EventHub).Name,         
                };
            byPassApis.AddRange(AuthorizationLookupByPassApis.LookupByPass());
            return byPassApis;
        }

        public static Dictionary<string, List<string>> AnnonymousUserAccess()
        {
            var byPassApis = new Dictionary<string, List<string>>();
            return byPassApis;
        }



    }
}
