using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Linq;
using Rx.Core.Cache;
using Rx.Core.Data;
using Rx.Core.Security;
using Rx.Core.Security.Jwt;
using Rx.Core.Settings;
using System.Security.Principal;
using TravelApp.Api.SharedService;
using TravelApp.Domain;
using TravelApp.Infrastructure.ExceptionLogs;
using TravelApp.Infrastructure.Security;
using TravelApp.Infrastructure.Uow;
using TravelApp.Infrastructure.Utilities;

namespace TravelApp.Api
{
    public class CoreServices
    {
        public static void Register(IServiceCollection services, IConfiguration configuration, string path)
        {
            services.AddSingleton<IConfiguration>(_ => configuration);
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IPrincipal>(provider => provider.GetService<IHttpContextAccessor>().HttpContext.User);
            //Rx Core Services
            services.AddScoped<IRepositoryProvider, RepositoryProvider>();
            services.AddTransient<IJwtTokenProvider, JwtTokenProvider>();
            services.AddTransient<ITokenValidator, JwtTokenProvider>();
            services.AddScoped<IRequestAccessor, RequestAccessor>();
            
            services.AddScoped(typeof(IDbContextManager<>), typeof(DbContextManager<>));
            services.AddMemoryCache();
            RegisterApplicationService(services, configuration);
            RegisterSingleton(services, path);

        }
        public static void RegisterApplicationService(IServiceCollection services, IConfiguration configuration)
        {
            //Application Services
            //  services.AddScoped<ITravelAppUow, TravelAppUow>();
            services.AddTransient<IApplicationAuthorization, ApplicationAuthorization>();
        }


        public static void RegisterSingleton(IServiceCollection services, string path)
        {
            var serverSetting = new ServerSetting(JObject.Parse(GetJson(path, "server-settings")));
            services.AddSingleton<ServerSetting>(_ => serverSetting);
            services.AddSingleton<IApplicationCache, ApplicationCache>();
            services.AddScoped<ILogException, LogException>();
            services.AddScoped<IApplicationUtility, ApplicationUtility>();
            services.AddScoped<IFileOperations, FileOperations>();
            services.AddScoped<ICommunication, Communication>();
            services.AddScoped<IPasswordHash, PasswordHash>();
            TaskProcess.Start(path, serverSetting);
        }

        private static string GetJson(string path, string fileName)
        {
            return System.IO.File.ReadAllText(string.Format(@"{0}\{1}.json", path, fileName));
        }
    }
}
