using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using Rx.Core.Cache;
using Rx.Core.Security;
using Rx.Core.Settings;
using System.IO.Compression;
using TravelApp.Api.SharedService;
using TravelApp.Infrastructure.Context;
using TravelApp.Infrastructure.Security;

namespace TravelApp.Api
{

    public class ConfigurationService
    {
        public static void Register(IServiceCollection services, IConfiguration configuration, string path)
        {
            SetCQRSPolicy(services, configuration);
            SetGzipCompression(services);
            AddDbContext(services);
            CoreServices.Register(services, configuration, path);
            ContextServices.Register(services, configuration);
            UnitOfWorkServices.Register(services, configuration);
            DomainServices.Register(services);
            AddMvcOptions(services);
            services.AddSignalR();
        }

        private static void SetCQRSPolicy(IServiceCollection services, IConfiguration configuration) {
            var myArray = configuration.GetSection("Security:AllowedHosts").Get<string[]>();
            //services.AddCors(options =>
            //{
            //    options.AddPolicy("CorsPolicy",
            //        builder => builder
            //            .AllowAnyOrigin()
            //            .AllowAnyMethod()
            //            .AllowAnyHeader()
            //            .AllowCredentials());
            //});
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.WithOrigins(myArray)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });
        }

        private static void SetGzipCompression(IServiceCollection services) {
            services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Fastest);
            services.AddResponseCompression(options =>
            {
                options.Providers.Add<GzipCompressionProvider>();
            });
        }

        static void AddDbContext(IServiceCollection services) {
            services.AddDbContext<MainSqlDbContext>();
        }

        static void AddMvcOptions(IServiceCollection services) {
            services.AddMemoryCache();
            services.AddMvc(options =>
            {
                ///var applicationCache = services.BuildServiceProvider().GetService<IApplicationCache>();
               // var serverSetting = services.BuildServiceProvider().GetService<ServerSetting>();
                options.InputFormatters.Insert(0, new ApplicationInputFormatter());
                //options.Filters.Add(new ModelValidation());
                options.Filters.Add(new ModelValidationFilter());
                //options.Filters.Add(new LogRequest());
              //  options.Filters.Add(new DateTimeZoneFilter());
                //options.Filters.Add(new ApplicationETag(applicationCache,serverSetting));
            }).AddDataAnnotationsLocalization().AddJsonOptions(
                oo =>
                {
                    var resolver = new CamelCasePropertyNamesContractResolver();
                    if (resolver != null)
                    {
                        var res = resolver as DefaultContractResolver;
                        res.NamingStrategy = null;
                    }
                    oo.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                });
        }
    }
}
