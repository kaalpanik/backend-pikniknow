using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TravelApp.Infrastructure.Uow;

namespace TravelApp.Api
{
    public class UnitOfWorkServices
    {
        public static void Register(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<ITravelAppUow, TravelAppUow>();
            services.AddScoped<IApplicationExceptionUow, ApplicationExceptionUow>();
        }
    }
}