using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TravelApp.Infrastructure.Context;

namespace TravelApp.Api
{
    public class ContextServices
    {
        public static void Register(IServiceCollection services,IConfiguration configuration)
        {
            services.AddScoped<ITravelAppContext, TravelAppContext>();
            services.AddScoped<IExceptionLogContext, ExceptionLogContext>();
        }
    }
}