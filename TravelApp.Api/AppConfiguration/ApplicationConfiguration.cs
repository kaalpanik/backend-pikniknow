using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using Rx.Core.Security;
using System;
using System.Net;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Internal;
using Microsoft.AspNetCore.Http.Connections;
using TravelApp.Infrastructure.ExceptionLogs;
using TravelApp.Model.ViewModel;
using TravelApp.Api.AppConfiguration;
using System.Net.WebSockets;
using TravelApp.Infrastructure.Utilities;

namespace TravelApp.Api
{

    public class ApplicationConfiguration
    {
        public static void Configure(IApplicationBuilder applicationBuilder, IHostingEnvironment hostingEnvironment, IServiceProvider serviceProvider)
        {
            applicationBuilder.UseCors("CorsPolicy");

            if (hostingEnvironment.IsDevelopment())
            {
                applicationBuilder.UseDeveloperExceptionPage();
            }
            RegisterGlobalException(applicationBuilder);

            ApplySecurityHeaders(applicationBuilder);

            
            ApplyTokenSecurity(applicationBuilder, serviceProvider);

            applicationBuilder.UseResponseCompression();
            applicationBuilder.UseStaticFiles();
            applicationBuilder.UseMvc();
            //applicationBuilder.UseSignalR(hubs =>
            //{
            //    hubs.MapHub<EventHub>("/eventHub");
            //});

            //applicationBuilder.UseSignalR(routes => { routes.MapHub<EventHub>("/eventHub"); });
            applicationBuilder.UseSignalR(routes => { routes.MapHub<WebSocketEventHub>("/eventHub"); });

            //applicationBuilder.UseSignalR(endpoints =>
            //{
            //    endpoints.MapHub<EventHub>("/eventHub", options =>
            //    {
            //        options.Transports =
            //            HttpTransportType.WebSockets |
            //            HttpTransportType.LongPolling;
            //    });
            //});
        }

        static void ApplySecurityHeaders(IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.Use((context, next) =>
            {
                context.Response.Headers["X-XSS-Protection"] = "1; mode=block";
                context.Response.Headers["X-Frame-Options"] = "SameOrigin";
                context.Response.Headers["X-Content-Type-Options"] = "nosniff";
                context.Response.Headers["Strict-Transport-Security"] = "max-age=31536000";
                context.Response.Headers["Access-Control-Expose-Headers"] = "x-session";
                return next();
            });
            applicationBuilder.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
        }

        static void ApplyTokenSecurity(IApplicationBuilder applicationBuilder, IServiceProvider serviceProvider)
        {
            applicationBuilder.UseJwtAuthentication(
            ApplicationApi.AuthenitcationByPass(),
            ApplicationApi.AuthorizationByPass(),
            ApplicationApi.AnnonymousUserAccess()
            );
        }

        static void RegisterGlobalException(IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.UseExceptionHandler(
                 options =>
                 {
                     options.Run(
                     async context =>
                     {
                         //here bhavik
                         context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                         context.Response.ContentType = "application/json";
                         IExceptionHandlerFeature ex = context.Features.Get<IExceptionHandlerFeature>();
                         if (ex != null)
                         {
                             if (ex.Error.Message == "Token is not available in database" || ex.Error.Message == "Token is expired")
                             {
                                 context.Response.StatusCode = (int)HttpStatusCode.Gone;
                                 context.Response.ContentType = "application/json";
                                 string err = ex.Error.Message;
                                 string jsonResult = JsonConvert.SerializeObject(new APIResponse() {StatusCode = Model.SystemEnums.TravelAppEnums.APIResponseEnum.TokenExpired,  Status = false, Message = err }, new JsonSerializerSettings
                                 {
                                     ContractResolver = new CamelCasePropertyNamesContractResolver()
                                 });
                                 await context.Response.Body.WriteAsync(Encoding.ASCII.GetBytes(jsonResult), 0, jsonResult.Length).ConfigureAwait(false);
                             }
                             else
                             {
                                 ILogException logException = (ILogException)context.RequestServices.GetService(typeof(ILogException));
                                 string err = logException.Log(ex.Error, context.Request.Path.Value);
                                 await context.Response.Body.WriteAsync(Encoding.ASCII.GetBytes(err), 0, err.Length).ConfigureAwait(false);
                             }
                         }
                     });
                 });
        }
    }
}
