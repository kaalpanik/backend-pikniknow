using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TravelApp.Domain;

namespace TravelApp.Api
{
    public class DomainServices
    {
        public static void Register(IServiceCollection services)
        {
            services.AddScoped<IUserAuthenticationDomain, UserAuthenticationDomain>();
            services.AddScoped<IStoryDomain, StoryDomain>();
            services.AddScoped<IUserDomain, UserDomain>();
            services.AddScoped<ILookupsDomain, LookupsDomain>();
            services.AddScoped<IStoryCommentDomain, StoryCommentDomain>();
            services.AddScoped<IUserFollowerDomain, UserFollowerDomain>();
            services.AddScoped<IBannerDomain, BannerDomain>();
            services.AddScoped<IPageDomain, PageDomain>();
        }
    }
}