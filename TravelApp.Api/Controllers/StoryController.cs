﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TravelApp.Api.AppConfiguration;
using TravelApp.Domain;
using TravelApp.Model.DbModel;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoryController : BaseController
    {
        private IHubContext<EventHub> HubContext { get; set; }
        public StoryController(IStoryDomain storyDomain, IHubContext<EventHub> hubContext)
        {
            StoryDomain = storyDomain;
            HubContext = hubContext;
        }
        public IActionResult Get(int userId, string searchQuery,string orderByColumn, string sortOrder, int pageIndex, int rowCount, int storyId)
        {
            var result = StoryDomain.Get(userId, searchQuery, orderByColumn, sortOrder, pageIndex, rowCount, storyId);
            APIResponse = new APIResponse
            {
                Response = result,
                Status = true,
                StatusCode = APIResponseEnum.Success,
                Message = "Story list"
            };
            return Ok(APIResponse);
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Story story = StoryDomain.Get(id);
            APIResponse = new APIResponse
            {
                Response = story,
                Status = true,
                StatusCode = APIResponseEnum.Success,
                Message = "Story Detail"
            };
            return Ok(APIResponse);
        }

        [Route("GetLikeUserStory")]
        [HttpGet]
        public IActionResult GetLikeUserStory(int userId, int pageIndex, int rowCount)
        {
            var result = StoryDomain.GetLikeUserStory(userId, "", "", pageIndex, rowCount);
            APIResponse = new APIResponse
            {
                Response = result,
                Status = true,
                StatusCode = APIResponseEnum.Success,
                Message = "Story Like List"
            };
            return Ok(APIResponse);
        }

        [Route("GetStoryLikes")]
        [HttpGet]
        public IActionResult GetStoryLikes(int storyId, int pageIndex, int rowCount,int userId = 0)
        {
            var result = StoryDomain.GetStoryLikes(storyId, "", "", pageIndex, rowCount, userId);
            APIResponse = new APIResponse
            {
                Response = result,
                Status = true,
                StatusCode = APIResponseEnum.Success,
                Message = "Story Like List"
            };
            return Ok(APIResponse);
        }

        [HttpPost]
        public IActionResult Post([FromForm]Story story)
        {
            APIResponse = new APIResponse();
            HashSet<string> validations = StoryDomain.AddValidation(story);
            if (validations.Count() == 0)
            {
                string message = string.Empty;
                Story result = StoryDomain.Add(story);
                APIResponse = new APIResponse
                {
                    Response = result,
                    StatusCode = APIResponseEnum.Success,
                    Status = true,
                    Message = "Story Added"
                };
                return Ok(APIResponse);
            }
            APIResponse.Message = validations.FirstOrDefault();
            APIResponse.Status = false;
            APIResponse.StatusCode = APIResponseEnum.Failed;
            return BadRequest(APIResponse);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromForm]Story story)
        {
            APIResponse = new APIResponse();
            HashSet<string> validations = StoryDomain.UpdateValidation(story);
            if (validations.Count() == 0)
            {
                string message = string.Empty;
                Story result = StoryDomain.Update(story);
                APIResponse = new APIResponse
                {
                    Response = result,
                    StatusCode = APIResponseEnum.Success,
                    Status = true,
                    Message = "Story updated"
                };
                return Ok(APIResponse);
            }
            APIResponse.Message = validations.FirstOrDefault();
            APIResponse.Status = false;
            APIResponse.StatusCode = APIResponseEnum.Failed;
            return BadRequest(APIResponse);
        }

        [Route("Update/{id}")]
        [HttpPost("Update/{id}")]
        public IActionResult Update(int id, [FromForm]Story story)
        {
            APIResponse = new APIResponse();
            HashSet<string> validations = StoryDomain.UpdateValidation(story);
            if (validations.Count() == 0)
            {
                string message = string.Empty;
                Story result = StoryDomain.Update(story);
                APIResponse = new APIResponse
                {
                    Response = result,
                    StatusCode = APIResponseEnum.Success,
                    Status = true,
                    Message = "Story updated"
                };
                return Ok(APIResponse);
            }
            APIResponse.Message = validations.FirstOrDefault();
            APIResponse.Status = false;
            APIResponse.StatusCode = APIResponseEnum.Failed;
            return BadRequest(APIResponse);
        }

        [HttpPatch("{id}")]
        public IActionResult Patch(int id, [FromBody] Story story)
        {
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return Ok();
        }

        [Route("ManageStory")]
        [HttpPost]
        public IActionResult ManageStory([FromBody]StoryViewModel storyViewModel)
        {
            APIResponse = new APIResponse();
            string message = string.Empty;
            var result = StoryDomain.ManageStory(storyViewModel);
            APIResponse = new APIResponse
            {
                Response = result,
                StatusCode = APIResponseEnum.Success,
                Status = true,
                Message = "Success"
            };
            HubContext.Clients.All.SendAsync("story", result);
            return Ok(APIResponse);
        }

        [Route("UpdateStory")]
        [HttpPost]
        public IActionResult UpdateStory([FromBody]StoryViewModel storyViewModel)
        {
            APIResponse = new APIResponse();
            string message = string.Empty;
            var result = StoryDomain.UpdateStory(storyViewModel);
            APIResponse = new APIResponse
            {
                Response = result,
                StatusCode = APIResponseEnum.Success,
                Status = true,
                Message = "Success"
            };
            return Ok(APIResponse);
        }
    }
}
