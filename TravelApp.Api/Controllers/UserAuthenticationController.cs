﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FirebaseAdmin;
using FirebaseAdmin.Auth;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using TravelApp.Domain;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserAuthenticationController : BaseController
    {
        private IUserAuthenticationDomain UserAuthenticationDomain { get; set; }
        public UserAuthenticationController(IUserAuthenticationDomain userAuthenticationDomain)
        {
            UserAuthenticationDomain = userAuthenticationDomain;
            APIResponse = new APIResponse();
        }
        [HttpPost("login", Name = "login")]
        public IActionResult PostLogin([FromBody]UserCredentialViewModel userCredential)
        {
            APIResponse = new APIResponse();
            userCredential.IPAddress = GetIPAddress();
            var validations = UserAuthenticationDomain.PostLoginValidation(userCredential);
            if (validations.Count() == 0)
            {
                var result = UserAuthenticationDomain.PostLogin(userCredential);
                APIResponse.Status = true;
                APIResponse.Message = "Login successfully";
                APIResponse.StatusCode = APIResponseEnum.Success;
                APIResponse.Response = result;
                return Ok(APIResponse);
            }
            else
            {
                APIResponse.Response = validations;
                APIResponse.Message = validations.FirstOrDefault();
                APIResponse.StatusCode = APIResponseEnum.Failed;
                APIResponse.Status = false;
                return BadRequest(APIResponse);
            }
        }
        [HttpPost("OtpVerification", Name = "OtpVerification")]
        public IActionResult OtpVerification([FromBody]UserCredentialViewModel userCredential)
        {
            APIResponse = new APIResponse();
            userCredential.IPAddress = GetIPAddress();
            var validations = UserAuthenticationDomain.OtpVerificationValidation(userCredential);
            if (validations.Count() == 0)
            {
                var result = UserAuthenticationDomain.OtpVerification(userCredential);
                APIResponse.Status = true;
                APIResponse.Message = "Otp send on mobile number";
                APIResponse.Response = result;
                APIResponse.StatusCode = APIResponseEnum.Success;
                return Ok(APIResponse);
            }
            else
            {
                APIResponse.Response = validations;
                APIResponse.Message = validations.FirstOrDefault();
                APIResponse.StatusCode = APIResponseEnum.Failed;
                APIResponse.Status = false;
                return BadRequest(APIResponse);
            }
        }

        [HttpPost("EmailVerification", Name = "EmailVerification")]
        public IActionResult EmailVerification([FromBody]UserCredentialViewModel userCredential)
        {
            APIResponse = new APIResponse();
            userCredential.IPAddress = GetIPAddress();
            var validations = UserAuthenticationDomain.EmailVerificationValidation(userCredential);
            if (validations.Count() == 0)
            {
                UserAuthenticationDomain.EmailVerification(userCredential);
                APIResponse.Status = true;
                APIResponse.Message = "Verification link sent on your email.";
                APIResponse.StatusCode = APIResponseEnum.Success;
                return Ok(APIResponse);
            }
            else
            {
                APIResponse.Response = validations;
                APIResponse.Message = validations.FirstOrDefault();
                APIResponse.StatusCode = APIResponseEnum.Failed;
                APIResponse.Status = false;
                return BadRequest(APIResponse);
            }
        }

        [HttpPost("ResetPassword", Name = "ResetPassword")]
        public IActionResult ResetPassword([FromBody]UserCredentialViewModel userCredential)
        {
            APIResponse = new APIResponse();
            userCredential.IPAddress = GetIPAddress();
            var validations = UserAuthenticationDomain.ResetPasswordValidation(userCredential);
            if (validations.Count() == 0)
            {
                UserAuthenticationDomain.ResetPassword(userCredential);
                APIResponse.Status = true;
                APIResponse.Message = "Password changed successfully";
                APIResponse.StatusCode = APIResponseEnum.Success;
                return Ok(APIResponse);
            }
            else
            {
                APIResponse.Response = validations;
                APIResponse.Message = validations.FirstOrDefault();
                APIResponse.StatusCode = APIResponseEnum.Failed;
                APIResponse.Status = false;
                return BadRequest(APIResponse);
            }
        }

        private string GetIPAddress()
        {
            string IP4Address = String.Empty;
            IP4Address = HttpContext.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress.ToString();
            if (IP4Address != String.Empty)
            {
                return IP4Address;
            }
            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }
            return IP4Address;
        }
    }
}
