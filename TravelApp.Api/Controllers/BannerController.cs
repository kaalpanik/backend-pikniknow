﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TravelApp.Api.AppConfiguration;
using TravelApp.Domain;
using TravelApp.Model.DbModel;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BannerController : BaseController
    {
        public BannerController(IBannerDomain bannerDomain)
        {
            BannerDomain = bannerDomain;
        }

        public IActionResult Get()
        {
            var result = BannerDomain.Get();
            APIResponse = new APIResponse
            {
                Response = result,
                Status = true,
                StatusCode = APIResponseEnum.Success,
                Message = "Banner list"
            };
            return Ok(APIResponse);
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result = BannerDomain.Get(id);
            APIResponse = new APIResponse
            {
                Response = result,
                Status = true,
                StatusCode = APIResponseEnum.Success,
                Message = "Banner Detail"
            };
            return Ok(APIResponse);
        }


        [HttpPost]
        public IActionResult Post([FromForm]Banner banner)
        {
            APIResponse = new APIResponse();
            HashSet<string> validations = BannerDomain.AddValidation(banner);
            if (validations.Count() == 0)
            {
                string message = string.Empty;
                var result = BannerDomain.Add(banner);
                APIResponse = new APIResponse
                {
                    Response = result,
                    StatusCode = APIResponseEnum.Success,
                    Status = true,
                    Message = "banner added"
                };
               
                return Ok(APIResponse);
            }
            APIResponse.Message = validations.FirstOrDefault();
            APIResponse.Status = false;
            APIResponse.StatusCode = APIResponseEnum.Failed;
            return BadRequest(APIResponse);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromForm]Banner banner)
        {
            APIResponse = new APIResponse();
            HashSet<string> validations = BannerDomain.UpdateValidation(banner);
            if (validations.Count() == 0)
            {
                string message = string.Empty;
                var result = BannerDomain.Update(banner);
                APIResponse = new APIResponse
                {
                    Response = result,
                    StatusCode = APIResponseEnum.Success,
                    Status = true,
                    Message = "banner updated"
                };
                return Ok(APIResponse);
            }
            APIResponse.Message = validations.FirstOrDefault();
            APIResponse.Status = false;
            APIResponse.StatusCode = APIResponseEnum.Failed;
            return BadRequest(APIResponse);
        }

        [HttpPatch("{id}")]
        public IActionResult Patch(int id, [FromBody] Story story)
        {
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            APIResponse = new APIResponse();
            HashSet<string> validations = BannerDomain.DeleteValidation(id);
            if (validations.Count() == 0)
            {
                string message = string.Empty;
                BannerDomain.Delete(id);
                APIResponse = new APIResponse
                {
                    Response = null,
                    StatusCode = APIResponseEnum.Success,
                    Status = true,
                    Message = "Banner deleted"
                };
                return Ok(APIResponse);
            }
            APIResponse.Message = validations.FirstOrDefault();
            APIResponse.Status = false;
            APIResponse.StatusCode = APIResponseEnum.Failed;
            return BadRequest(APIResponse);
        }
    }
}
