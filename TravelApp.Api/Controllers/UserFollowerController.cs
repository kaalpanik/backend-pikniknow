﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TravelApp.Domain;
using TravelApp.Model.DbModel;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserFollowerController : BaseController
    {
        public UserFollowerController(IUserFollowerDomain userFollowerDomain)
        {
            UserFollowerDomain = userFollowerDomain;
        }
        public IActionResult Get(string searchQuery, string orderByColumn, string sortOrder, int pageIndex, int rowCount, int userId,int typeId)
        {
            var result = UserFollowerDomain.Get(searchQuery, orderByColumn, sortOrder, pageIndex, rowCount, userId, typeId);
            APIResponse = new APIResponse
            {
                Response = result,
                Status = true,
                StatusCode = APIResponseEnum.Success,
                Message = "Data list"
            };
            return Ok(APIResponse);
        }

        [HttpPost]
        public IActionResult Post([FromBody]UserFollowerViewModel userFollowerViewModel)
        {
            APIResponse = new APIResponse();
            string message = "User added";
            var result = UserFollowerDomain.Operation(userFollowerViewModel);
            APIResponse = new APIResponse
            {
                Response = result,
                StatusCode = APIResponseEnum.Success,
                Status = true,
                Message = message
            };
            return Ok(APIResponse);
        }
    }
}
