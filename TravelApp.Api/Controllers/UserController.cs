﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TravelApp.Domain;
using TravelApp.Model.DbModel;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : BaseController
    {
        public UserController(IUserDomain userDomain)
        {
            UserDomain = userDomain;
        }
        public IActionResult Get(string orderByColumn, string sortOrder, int pageIndex, int rowCount, string searchQuery, int userId, int roleId)
        {
            var result = UserDomain.Get(orderByColumn, sortOrder, pageIndex, rowCount, searchQuery, userId, roleId);
            APIResponse = new APIResponse
            {
                Response = result,
                Status = true,
                StatusCode = APIResponseEnum.Success,
                Message = "Story list"
            };
            return Ok(APIResponse);
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            User user = UserDomain.Get(id);
            APIResponse = new APIResponse
            {
                Response = user,
                Status = true,
                StatusCode = APIResponseEnum.Success,
                Message = "User record Detail"
            };
            return Ok(APIResponse);
        }


        [HttpPost]
        public IActionResult Post([FromForm]User user)
        {
            APIResponse = new APIResponse();
            HashSet<string> validations = UserDomain.AddValidation(user);
            if (validations.Count() == 0)
            {
                string message = "User added";
                User result = UserDomain.Add(user);
                APIResponse = new APIResponse
                {
                    Response = result,
                    StatusCode = APIResponseEnum.Success,
                    Status = true,
                    Message = message
                };
                return Ok(APIResponse);
            }
            APIResponse.Message = validations.FirstOrDefault();
            APIResponse.Status = false;
            APIResponse.StatusCode = APIResponseEnum.Failed;
            return BadRequest(APIResponse);
        }

        [HttpPost("{id}")] // Due to flatter 
        public IActionResult Put(int id, [FromForm]User user)
        {
            APIResponse = new APIResponse();
            HashSet<string> validations = UserDomain.UpdateValidation(user);
            if (validations.Count() == 0)
            {
                string message = string.Empty;
                User result = UserDomain.Update(user);
                APIResponse = new APIResponse
                {
                    Response = result,
                    StatusCode = APIResponseEnum.Success,
                    Status = true,
                    Message = message
                };
                return Ok(APIResponse);
            }
            APIResponse.Message = validations.FirstOrDefault();
            APIResponse.Status = false;
            APIResponse.StatusCode = APIResponseEnum.Failed;
            return BadRequest(APIResponse);
        }

        [Route("UpdateProfile")]// Due to flatter 
        public IActionResult UpdateProfile([FromForm]User user)
        {
            APIResponse = new APIResponse();
            var validations = UserDomain.UpdateValidation(user);
            if (validations.Count() == 0)
            {
                string message = string.Empty;
                User result = UserDomain.Update(user);
                APIResponse = new APIResponse
                {
                    Response = result,
                    StatusCode = APIResponseEnum.Success,
                    Status = true,
                    Message = message
                };
                return Ok(APIResponse);
            }
            APIResponse.Message = validations.FirstOrDefault();
            APIResponse.Status = false;
            APIResponse.StatusCode = APIResponseEnum.Failed;
            return BadRequest(APIResponse);
        }

        [HttpPatch("{id}")]
        public IActionResult Patch(int id, [FromBody] User User)
        {
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            APIResponse = new APIResponse();
            HashSet<string> validations = UserDomain.DeleteValidation(id);
            if (validations.Count() == 0)
            {
                string message = string.Empty;
                UserDomain.Delete(id);
                APIResponse = new APIResponse
                {
                    StatusCode = APIResponseEnum.Success,
                    Status = true,
                    Message = "User deleted"
                };
                return Ok(APIResponse);
            }
            APIResponse.Message = validations.FirstOrDefault();
            APIResponse.Status = false;
            APIResponse.StatusCode = APIResponseEnum.Failed;
            return BadRequest(APIResponse);
        }

        [Route("UserNotification")]// Due to flatter 
        public IActionResult UserNotification(int userId, int type, int pageIndex, int rowCount)
        {
            APIResponse = new APIResponse();
            var staticPageList = UserDomain.UserNotification(userId, type, pageIndex, rowCount);
            APIResponse = new APIResponse
            {
                Response = staticPageList,
                Status = true,
                Message = "Notification List",
            };
            return Ok(APIResponse);
        }

    }
}

