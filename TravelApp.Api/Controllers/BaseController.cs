﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TravelApp.Domain;
using TravelApp.Infrastructure.Uow;
using TravelApp.Model.ViewModel;

namespace TravelApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ProducesResponseType(typeof(APIResponse), (int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(APIResponse), (int)HttpStatusCode.NoContent)]
    [ProducesResponseType(typeof(APIResponse), (int)HttpStatusCode.InternalServerError)]
    public class BaseController : Controller
    {
        public ITravelAppUow TravelAppUow { get; set; }
        public APIResponse APIResponse { get; set; } = new APIResponse();
        public IUserDomain  UserDomain { get; set; }
        public IStoryDomain StoryDomain { get; set; }

        public ILookupsDomain LookupsDomain { get; set; }

        public IStoryCommentDomain StoryCommentDomain { get; set; }

        public IUserFollowerDomain UserFollowerDomain { get; set; }

        public IPageDomain PageDomain { get; set; }

        public IBannerDomain BannerDomain { get; set; }

    }
}