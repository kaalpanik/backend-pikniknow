﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TravelApp.Api.AppConfiguration;
using TravelApp.Domain;
using TravelApp.Model.DbModel;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;


namespace TravelApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoryCommentController : BaseController
    {
        private IHubContext<EventHub> HubContext { get; set; }
        public StoryCommentController(IStoryCommentDomain storyCommentDomain, IHubContext<EventHub> hubContext)
        {
            StoryCommentDomain = storyCommentDomain;
            HubContext = hubContext;
        }
        public IActionResult Get(string searchQuery, string orderByColumn, string sortOrder, int pageIndex, int rowCount, int storyId)
        {
            var result = StoryCommentDomain.Get(searchQuery, orderByColumn, sortOrder, pageIndex, rowCount, storyId);
            APIResponse = new APIResponse
            {
                Response = result,
                Status = true,
                StatusCode = APIResponseEnum.Success,
                Message = "Story list"
            };
            return Ok(APIResponse);
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            StoryComment story = StoryCommentDomain.Get(id);
            APIResponse = new APIResponse
            {
                Response = story,
                Status = true,
                StatusCode = APIResponseEnum.Success,
                Message = "Story Comment Detail"
            };
            return Ok(APIResponse);
        }


        [HttpPost]
        public IActionResult Post([FromBody]StoryComment storyComment)
        {
            APIResponse = new APIResponse();
            HashSet<string> validations = StoryCommentDomain.AddValidation(storyComment);
            if (validations.Count() == 0)
            {
                string message = string.Empty;
                StoryComment result = StoryCommentDomain.Add(storyComment);
                APIResponse = new APIResponse
                {
                    Response = result,
                    StatusCode = APIResponseEnum.Success,
                    Status = true,
                    Message = "Story comment added"
                };
                var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                if (storyComment.ParentStoryCommentId == 0)
                {
                    HubContext.Clients.All.SendAsync("StoryComment", jsonString);
                }
                return Ok(APIResponse);
            }
            APIResponse.Message = validations.FirstOrDefault();
            APIResponse.Status = false;
            APIResponse.StatusCode = APIResponseEnum.Failed;
            return BadRequest(APIResponse);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]StoryComment storyComment)
        {
            APIResponse = new APIResponse();
            HashSet<string> validations = StoryCommentDomain.UpdateValidation(storyComment);
            if (validations.Count() == 0)
            {
                string message = string.Empty;
                StoryComment result = StoryCommentDomain.Update(storyComment);
                APIResponse = new APIResponse
                {
                    Response = result,
                    StatusCode = APIResponseEnum.Success,
                    Status = true,
                    Message = "Story comment updated"
                };
                return Ok(APIResponse);
            }
            APIResponse.Message = validations.FirstOrDefault();
            APIResponse.Status = false;
            APIResponse.StatusCode = APIResponseEnum.Failed;
            return BadRequest(APIResponse);
        }

        [HttpPatch("{id}")]
        public IActionResult Patch(int id, [FromBody] Story story)
        {
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            APIResponse = new APIResponse();
            HashSet<string> validations = StoryCommentDomain.DeleteValidation(id);
            if (validations.Count() == 0)
            {
                string message = string.Empty;
                StoryCommentDomain.Delete(id);
                APIResponse = new APIResponse
                {
                    Response = null,
                    StatusCode = APIResponseEnum.Success,
                    Status = true,
                    Message = "Story comment deleted"
                };
                return Ok(APIResponse);
            }
            APIResponse.Message = validations.FirstOrDefault();
            APIResponse.Status = false;
            APIResponse.StatusCode = APIResponseEnum.Failed;
            return BadRequest(APIResponse);
        }
    }
}
