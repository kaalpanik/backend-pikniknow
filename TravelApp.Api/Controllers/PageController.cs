﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using TravelApp.Api.AppConfiguration;
using TravelApp.Domain;
using TravelApp.Model.DbModel;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Api.Controllers
{
    [Route("api/[controller]")] 
    [ApiController]
    public class PageController : BaseController
    {
        public PageController(IPageDomain  pageDomain)
        {
            PageDomain = pageDomain;
        }

        public IActionResult Get()
        {
            var result = PageDomain.Get();
            APIResponse = new APIResponse
            {
                Response = result,
                Status = true,
                StatusCode = APIResponseEnum.Success,
                Message = "Page list"
            };
            return Ok(APIResponse);
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result = PageDomain.Get(id);
            APIResponse = new APIResponse
            {
                Response = result,
                Status = true,
                StatusCode = APIResponseEnum.Success,
                Message = "Page Detail"
            };
            return Ok(APIResponse);
        }

        [Route("GetByPageLocationId/{id}")]
        [HttpGet("GetByPageLocationId/{id}")]
        public IActionResult GetByPageLocationId(int id)
        {
            var result = PageDomain.GetPageByLocationId(id);
            APIResponse = new APIResponse
            {
                Response = result,
                Status = true,
                StatusCode = APIResponseEnum.Success,
                Message = "Page Detail"
            };
            return Ok(APIResponse);
        }


        [HttpPost]
        public IActionResult Post([FromBody]Page page)
        {
            APIResponse = new APIResponse();
            HashSet<string> validations = PageDomain.AddValidation(page);
            if (validations.Count() == 0)
            {
                string message = string.Empty;
                var result = PageDomain.Add(page);
                APIResponse = new APIResponse
                {
                    Response = result,
                    StatusCode = APIResponseEnum.Success,
                    Status = true,
                    Message = "Page added"
                };

                return Ok(APIResponse);
            }
            APIResponse.Message = validations.FirstOrDefault();
            APIResponse.Status = false;
            APIResponse.StatusCode = APIResponseEnum.Failed;
            return BadRequest(APIResponse);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Page page)
        {
            APIResponse = new APIResponse();
            HashSet<string> validations = PageDomain.UpdateValidation(page);
            if (validations.Count() == 0)
            {
                string message = string.Empty;
                var result = PageDomain.Update(page);
                APIResponse = new APIResponse
                {
                    Response = result,
                    StatusCode = APIResponseEnum.Success,
                    Status = true,
                    Message = "Page updated"
                };
                return Ok(APIResponse);
            }
            APIResponse.Message = validations.FirstOrDefault();
            APIResponse.Status = false;
            APIResponse.StatusCode = APIResponseEnum.Failed;
            return BadRequest(APIResponse);
        }

        [HttpPatch("{id}")]
        public IActionResult Patch(int id, [FromBody] Story story)
        {
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            APIResponse = new APIResponse();
            HashSet<string> validations = PageDomain.DeleteValidation(id);
            if (validations.Count() == 0)
            {
                string message = string.Empty;
                PageDomain.Delete(id);
                APIResponse = new APIResponse
                {
                    Response = null,
                    StatusCode = APIResponseEnum.Success,
                    Status = true,
                    Message = "Page deleted"
                };
                return Ok(APIResponse);
            }
            APIResponse.Message = validations.FirstOrDefault();
            APIResponse.Status = false;
            APIResponse.StatusCode = APIResponseEnum.Failed;
            return BadRequest(APIResponse);
        }
    }
}
