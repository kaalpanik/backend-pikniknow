﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TravelApp.Domain;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LookupsController : BaseController
    {
        public LookupsController(ILookupsDomain lookupsDomain)
        {
            LookupsDomain = lookupsDomain;
        }
        [HttpGet]
        public IActionResult Get(string searchQuery)
        {
            var result = LookupsDomain.Get(searchQuery);
            APIResponse = new APIResponse
            {
                Response = result,
                Status = true,
                StatusCode = APIResponseEnum.Success,
                Message = "Story list"
            };
            return Ok(APIResponse);
        }

        [Route("Dashboard")]
        [HttpGet]
        public IActionResult GetDashboard()
        {
            var result = LookupsDomain.GetDashboard();
            APIResponse = new APIResponse
            {
                Response = result,
                Status = true,
                StatusCode = APIResponseEnum.Success,
                Message = "Dashboard"
            };
            return Ok(APIResponse);
        }
    }
}
