﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TravelApp.Infrastructure.Security;
using TravelApp.Model.ViewModel;
using static TravelApp.Model.SystemEnums.TravelAppEnums;

namespace TravelApp.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserAuthorizationController : BaseController
    {
        private IJwtTokenProvider TokenProvider { get; set; }
        public UserAuthorizationController(IJwtTokenProvider tokenProvider)
        {
            TokenProvider = tokenProvider;
            APIResponse = new APIResponse();
        }
        [HttpPost("logout",Name = "logout")]
        public IActionResult PostLogOut()
        {
            TokenProvider.LogOut();
            APIResponse.Response = null;
            APIResponse.Message = "Logout Successfully.";
            APIResponse.StatusCode = APIResponseEnum.Success;
            APIResponse.Status = true;
            return Ok(APIResponse);
        }
    }
}