using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace TravelApp.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public string Path { get; set; }
        public void ConfigureServices(IServiceCollection services)
        {
            Path = System.IO.Directory.GetCurrentDirectory();

            ConfigurationService.Register(services, Configuration, Path);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            ApplicationConfiguration.Configure(app, env, serviceProvider);
        }
        private static string GetJson(string path, string fileName)
        {
            return System.IO.File.ReadAllText(string.Format(@"{0}\{1}.json", path, fileName));
        }
    }
}
